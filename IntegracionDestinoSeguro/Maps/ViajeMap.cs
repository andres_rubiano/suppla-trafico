﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DestinoSeguro.Dtos;
using IntegracionDestinoSeguro.Models;
using Simplexity.Traffic.EF;

namespace IntegracionDestinoSeguro.Maps
{
  public class ViajeMap
  {
    public ViajeDTO Viaje(Viaje value)
    {
      
      var viajeDTO = new ViajeDTO
      {
         Id= value.Id,
         FechaSalida =value.FechaSalida,
         NitTransportador = value.NitTransportador ?? "",
         Manifiesto = value.Manifiesto ?? "",
         CodigoOrigen = value.CodigoOrigen ?? "",
         CodigoDestino = value.CodigoDestino ?? "",
         CodigoVia = value.CodigoVia ?? "",
         CodigoRuta = value.CodigoRuta ?? "",
         Placa = value.Placa ?? "",
         CodigoMarca = value.CodigoMarca ?? "",
         CodigoCarroceria = value.CodigoCarroceria ?? "",
         CodigoColor = value.CodigoColor ?? "",
         Modelo = value.Modelo ?? "",
         CedulaConductor = value.CedulaConductor ?? "",
         NombresConductor = value.NombresConductor ?? "",
         ApellidosConductor = value.ApellidosConductor ?? "",
         Direccion = value.Direccion ?? "",
         Telefono = value.Telefono ?? "",
         Celular = value.Celular ?? "",
         NitGenerador = value.NitGenerador ?? "",
         NombreGenerador = value.NombreGenerador ?? "",
         Sucursal = value.Sucursal ?? "",
         Observaciones = value.Observaciones ?? ""
      };
      return viajeDTO;
    }
  }
}
