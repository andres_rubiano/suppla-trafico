﻿using Topshelf;

namespace IntegracionDestinoSeguro
{
  class Program
  {
    static void Main(string[] args)
    {

#if DEBUG
      var notificationService = new NotificationService();
      notificationService.Procesar();

#else
      HostFactory.Run(x =>
      {
        x.Service<NotificationService>(s =>
        {
          s.ConstructUsing(name => new NotificationService());
          s.WhenStarted(tc => tc.Start());
          s.WhenStopped(tc => tc.Stop());
        });
        x.StartAutomaticallyDelayed();
        x.RunAsLocalSystem();
        x.SetDescription("IntegracionDestinoSeguro.NotificationService");
        x.SetDisplayName("IntegracionDestinoSeguro.NotificationService");
        x.SetServiceName("IntegracionDestinoSeguro.NotificationService");
      });

       //1. Timer
       //2. Clase que hace la consulta

#endif


    }
  }
}
