﻿using System;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;
using DestinoSeguro.Dtos;

namespace IntegracionDestinoSeguro
{
  public class LogRecord<T>
    {
      private T _object;

      public LogRecord(T t)
        {
          this._object = t;
        }

      public void WriteLog(string fileName)
        {
          if (ConfigurationManager.AppSettings["CreateLog"] != "True")
            {
              return;
            }

          string path = System.AppDomain.CurrentDomain.BaseDirectory + @"logs\";
          if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
        
          string pathFileName = string.Empty;

          if (_object.GetType() == typeof (ViajeDTO))
            {
              pathFileName = string.Format(path + "{0}.txt", (fileName ?? "NoFileName") + "_" + DateTime.Now.ToString("yyyymmdd_HHmmss"));
            }

          FileStream fs = new FileStream(pathFileName, FileMode.OpenOrCreate);
          StreamWriter writer = new StreamWriter(fs);
          XmlSerializer dtoXml = new XmlSerializer(typeof(T));

          dtoXml.Serialize(writer, _object);

          fs.Close();
        }
    }

}
