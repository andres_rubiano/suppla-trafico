﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionDestinoSeguro.Models
{
  public class Viaje
  {
    public int Id {get; set; } 
    public DateTime FechaSalida { get; set; }
    public string NitTransportador { get; set; }
    public string Manifiesto { get; set; }
    public string CodigoOrigen { get; set; }
    public string CodigoDestino { get; set; }
    public string CodigoVia { get; set; }
    public string CodigoRuta { get; set; }
    public string Placa { get; set; }
    public string CodigoMarca { get; set; }
    public string CodigoCarroceria { get; set; }
    public string CodigoColor { get; set; }
    public string Modelo { get; set; }
    public string CedulaConductor { get; set; }
    public string NombresConductor { get; set; }
    public string ApellidosConductor { get; set; }
    public string Direccion { get; set; }
    public string Telefono { get; set; }
    public string Celular { get; set; }
    public string NitGenerador { get; set; }
    public string NombreGenerador { get; set; }
    public string Sucursal { get; set; }
    public string Observaciones { get; set; }
  }
}
