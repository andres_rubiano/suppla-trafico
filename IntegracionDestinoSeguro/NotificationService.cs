﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Timers;
using System.Xml.Serialization;
using DestinoSeguro;
using DestinoSeguro.Dtos;
using DestinoSeguro.Settings;
using IntegracionDestinoSeguro.Maps;
using IntegracionDestinoSeguro.Models;
using Microsoft.JScript;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Simplexity.Components.Messaging;
using Simplexity.Traffic.Constants;
using Simplexity.Traffic.EF;
using Convert = Microsoft.JScript.Convert;
using System.Timers;

namespace IntegracionDestinoSeguro
{
  public class NotificationService
  {
    private IMessageReceiver<Viaje> _viajeMessageReceiver;
    private int _interval;
    private Timer _timer;
    public void Start()
    {
      try
      {
        if (!int.TryParse(ConfigurationManager.AppSettings["parMatchExecInterval"], out _interval))
        {
          _interval = 50000;
        }

        _timer = new Timer();
        _timer.Elapsed += MatchTimerElapsed;
        _timer.Interval = _interval;
        _timer.Enabled = true;
        _timer.AutoReset = true;
      }
      catch (Exception ex)
      {
        ExceptionPolicy.HandleException(ex, "Exception Shielding");
      }
    }

    private void MatchTimerElapsed(object sender, ElapsedEventArgs e)
    {
      _timer.Enabled = false;
      Procesar();
      _timer.Enabled = true;
    }
    public void Stop()
    {
      if (_viajeMessageReceiver == null) return;

      _viajeMessageReceiver.StopAsync();
      _viajeMessageReceiver.Dispose();
    }

    public void Procesar()
    {
      // 1. Verificar si hay CX a DS

      try
      {
        // 1. Leer la tabla TfRequestService y hacer join con la tabla tfloads para obtener los datos del viaje que se va a reportar cuando el estado sea 0

        var listTfRequestService = ObtenerRegistros();

        foreach (var tfRequestService in listTfRequestService)
        {
          // Esto devuelve 0 o n registrors, por cada uno de ellos:


          // 1.1 Hacer un mapper del registro a viajeDTO
          var viaje = new ViajeMap();

          var viajeDTO = viaje.Viaje(tfRequestService);

          // Se envia registro a log:
          var viajeDtoLog = new LogRecord<ViajeDTO>(viajeDTO);
          viajeDtoLog.WriteLog(viajeDTO.Manifiesto);

          // 1.2 Reportar a DS el registro
          WebClient client = new WebClient();
          try
          {
            //evalua si hay conexion con destino seguro
            using (client.OpenRead(App.DestinoSeguroConfiguration.Address))
            {
            }
          
            var cliente = new Cliente(new Uri(App.DestinoSeguroConfiguration.Address), App.DestinoSeguroConfiguration.User,
              App.DestinoSeguroConfiguration.Password);
            var respuesta = cliente.ReportarViaje(viajeDTO);

            // 1.3 Si DS response exitosamente se actualiza el registro a procesado, si responde negativamente procesado con error
            var db1 = new TrafficEntities();
            var sqlRequesteService = (from viaje1 in db1.TfRequestService
                                      where viaje1.TfrId == viajeDTO.Id
                                      select viaje1).FirstOrDefault();
            var sqlLoads = (from loads in db1.tfLoads
                            where loads.LtfLoad_LoaNumber == viajeDTO.Manifiesto
                            select loads).FirstOrDefault();

            //separa el mensaje que se obtiene desde destino seguro para obtener el numero de registro por aparte para poder almacenarlo en la base de datos 
            var rest = respuesta.Message.Split('-');

            if (respuesta.Type == 1)
            {
              if (sqlRequesteService != null)
              {
                sqlRequesteService.TfrState = 2;
                sqlRequesteService.TfrResponse = respuesta.Message;
                sqlRequesteService.TfrProcessedDate = DateTime.Now;
                sqlLoads.LtfIntegrationDate = DateTime.Now;
                sqlLoads.LtfNumberIntegration = Convert.ToInt32(rest[1]);
              }


              db1.SaveChanges();
            }


            //  1.4 Si no hay comunicación, procesado con error y se escribe en el response el motivo

            if (respuesta.Type == 3)
            {
              if (sqlRequesteService != null)
              {
                sqlRequesteService.TfrState = -99;
                sqlRequesteService.TfrResponse = respuesta.Message;
                sqlRequesteService.TfrProcessedDate = DateTime.Now;
                //sqlLoads.LtfIntegrationDate = DateTime.Now;
              }


              db1.SaveChanges();
            }
          }
          catch (WebException ex)
          {
            ExceptionPolicy.HandleException(ex, "Exception Shielding");
          }
          // Continua con el siguiente registro

        }
      }
      catch (Exception ex) 
      {
        ExceptionPolicy.HandleException(ex, "Exception Shielding");
        
      }
        
      
   

    }

    private IEnumerable<Viaje> ObtenerRegistros()
    {
      IEnumerable<Viaje> listViajes;
      try
      {
       
        // obtiene los registro que se desean reportar a destino seguro
        using (var db = new TrafficEntities())
        {

          listViajes = (from via in db.TfRequestService
            join tfrLoads in db.tfLoads on via.TfrReference equals tfrLoads.LtfLoad_LoaNumber
            where via.TfrState == 0
            select new
            {
              via.TfrId,
              tfrLoads.LtfStartDate,
              tfrLoads.LtfLoad_LoaNumber,
              tfrLoads.LtfLoaFrom_CitCode,
              tfrLoads.LtfLoaTo_CitCode,
              tfrLoads.LtfRouteVia,
              tfrLoads.LtfRoute_ExternalCode,
              tfrLoads.LtfTruck_VehCode,
              tfrLoads.LtfDriver_UcrCode,
              tfrLoads.LtfDriverName,
              tfrLoads.LtfDriverLastName,
              tfrLoads.LtfLoaCustomerIdentification,
              tfrLoads.LtfLoaCustomerNames,
              tfrLoads.LtfSubsidiary_SubCode
            }).AsEnumerable().Select(x => new Viaje()
            {
              Id = x.TfrId,
              FechaSalida = (DateTime) x.LtfStartDate,
              Manifiesto = x.LtfLoad_LoaNumber,
              NitTransportador = App.DestinoSeguroConfiguration.Nittransportadora,
              CodigoOrigen = x.LtfLoaFrom_CitCode,
              CodigoDestino = x.LtfLoaTo_CitCode,
              CodigoVia = x.LtfRouteVia != null ? x.LtfRouteVia.ToString() : string.Empty,
              CodigoRuta = x.LtfRoute_ExternalCode,
              Placa = x.LtfTruck_VehCode,
              CedulaConductor = x.LtfDriver_UcrCode,
              NombresConductor = x.LtfDriverName,
              ApellidosConductor = x.LtfDriverLastName,
              NitGenerador = x.LtfLoaCustomerIdentification,
              NombreGenerador = x.LtfLoaCustomerNames,
              Sucursal = x.LtfSubsidiary_SubCode
            }).ToList();

        }

        return listViajes;

      }
      catch (Exception ex)
      {

        ExceptionPolicy.HandleException(ex, "Exception Shielding");
        return null;
      }
      
      
    }
  }

  
}
