﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using DestinoSeguro;
using DestinoSeguro.Dtos;
using DestinoSeguro.Settings;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Simplexity.Traffic.DTOs;
using Simplexity.Traffic.EF;

namespace Simplexity.Traffic.Services
  {
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    [ExceptionShielding("WCF Exception Shielding")]
    public class TrafficServices : ITrafficServices
      {
        public ResponseDTO AddTrafficLoad(TrafficLoadDTO trafficLoad)
          {
            return new Traffic.TrafficServices().TrafficRoute_Initialize(trafficLoad);
          }

        public ResponseDTO EventRegister(EventDTO loadEventDto)
          {
            return new Traffic.TrafficServices().Event_Register(loadEventDto);
          }

        public List<VehiclesGpsDto> GetVehiclesInfo()
          {
            return new Traffic.TrafficServices().GetVehiclesInfo();
          }

        public ResponseDTO EndRoute(string loaNumber, string usrcode)
          {
            var loadEventDto = new EventDTO()
                                 {
                                   TfeLoad_LoaNumber = loaNumber,
                                   TfeCheckpoint_TrcCode = "Destino",
                                   TfeDate = DateTime.Now,
                                   TfeType = "ERT",
                                   TfeCreation_UsrCode = usrcode
                                 };
            return new Traffic.TrafficServices().Event_Register(loadEventDto);
          }

        public ResponseDTO CancelLoad(string loaNumber)
          {
            return new Traffic.TrafficServices().CancelLoad(loaNumber);
          }

        public RespuestaEventosDTO GetDestinoSeguroEvent(string loanumber, string vehicleCode, string usrCode)
          {
            var db1 = new TrafficEntities();
            var deleteDestinoSeguro = (from del in db1.tfLoadTrafficEvents
                                       where del.TfeLoad_LoaNumber == loanumber
                                             & del.TfeSource == 2
                                       select del);
            foreach (var destinoSeguro in deleteDestinoSeguro)
            {
              db1.DeleteObject(destinoSeguro);
            }
            //db1.SaveChanges();

            var cliente = new Cliente(new Uri(App.DestinoSeguroConfiguration.Address), App.DestinoSeguroConfiguration.User,
                                      App.DestinoSeguroConfiguration.Password);


            var eventosDestinoSeguro = cliente.ObtenerEventos(loanumber, vehicleCode, App.DestinoSeguroConfiguration.Nittransportadora);

          // DS devuelve mensajes que son :
          // No hay novedades para el vehículo

            var demo = eventosDestinoSeguro.MessageDto.Message;

            if (demo != null)
              {
                var response1 = demo.IndexOf("No hay novedades", System.StringComparison.Ordinal);
                if (response1 >= 0) return eventosDestinoSeguro;
              }
          
            if (eventosDestinoSeguro.Eventos != null)
            {
              #region Se obtuvieron eventos de DS:

              foreach (var eventoDestinoSeguroDto in eventosDestinoSeguro.Eventos)
                  {

                    // obtenemos el codigo del puesto de control y si esta registrado en los puestos del viaje:
                    var idCheckPoint = eventoDestinoSeguroDto.IdPuestoControl.ToString();
                  
                    var checkPoint = (from i in db1.tfLoadCheckPoints
                                      join tfCheckpoint in db1.tfCheckpoints on i.LitCheckpoint_TrcCode equals tfCheckpoint.TrcCode
                                      where i.LitLoad_LoaNumber == loanumber
                                            && tfCheckpoint.TrcExternalCode == idCheckPoint
                                      orderby i.LitEstimatedDate
                                      select i
                      ).FirstOrDefault();


                    if (eventoDestinoSeguroDto.TipoEvento == "P/C" && checkPoint != null)
                      {

                        EventRegister(
                          new EventDTO()
                            {
                              TfeCheckpoint_TrcCode = checkPoint.LitCheckpoint_TrcCode,
                              TfeTypeDescription = eventoDestinoSeguroDto.TipoEvento,
                              TfeLoad_LoaNumber = eventoDestinoSeguroDto.NumeroViaje,
                              TfTruck_VehCode = eventoDestinoSeguroDto.Placa,
                              TfeCreation_UsrCode = usrCode,
                              TfeType = "DestinoSeguroPC",
                              TfeDate = eventoDestinoSeguroDto.FechaEvento,
                              TfeText = eventoDestinoSeguroDto.NombreEvento + "|" + eventoDestinoSeguroDto.Observaciones,
                              TfeSource = 2,
                            }
                          );
                      }
                    else
                      {
                        EventRegister(
                          new EventDTO()
                            {
                              TfeCheckpoint_TrcCode = eventoDestinoSeguroDto.IdPuestoControl.ToString(),
                              TfeTypeDescription = eventoDestinoSeguroDto.TipoEvento,
                              TfeLoad_LoaNumber = eventoDestinoSeguroDto.NumeroViaje,
                              TfTruck_VehCode = eventoDestinoSeguroDto.Placa,
                              TfeCreation_UsrCode = usrCode,
                              TfeType = "DestinoSeguroSG",
                              TfeDate = eventoDestinoSeguroDto.FechaEvento,
                              TfeText = eventoDestinoSeguroDto.NombreEvento + "|" + eventoDestinoSeguroDto.Observaciones,
                              TfeSource = 2,
                            }
                          );
                      }
                  }

              #endregion

              if(eventosDestinoSeguro.Eventos.Count > 0) db1.SaveChanges();

            }
            else
              {
                var mensajerespuesta = eventosDestinoSeguro.MessageDto.Message;
              }
            return eventosDestinoSeguro;
          }
      }
  }