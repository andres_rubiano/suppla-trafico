﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using DestinoSeguro.Dtos;
using Simplexity.Traffic.DTOs;

namespace Simplexity.Traffic.Services
{
    [ServiceContract]
    public interface ITrafficServices
    {
        [OperationContract]
        //[WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ResponseDTO AddTrafficLoad(TrafficLoadDTO trafficLoad);

        [OperationContract]
        //[WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ResponseDTO EventRegister(EventDTO loadEventDto);

        [OperationContract]
        List<VehiclesGpsDto> GetVehiclesInfo();

        [OperationContract]
        //[WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        ResponseDTO CancelLoad(string loaNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json)]
        RespuestaEventosDTO GetDestinoSeguroEvent(string loanumber, string vehicleCode,string usrCode);
    }
}
