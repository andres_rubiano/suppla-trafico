﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Simplexity.Traffic.Services.ErrorHandler
{
  [DataContract]
  public class ServiceFaultException
  {
    [DataMember]
    public Guid FaultID { get; set; }

    [DataMember]
    public string FaultMessage { get; set; }
  }
}