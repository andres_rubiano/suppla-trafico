﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Simplexity.Traffic.DTOs;
using Simplexity.Traffic._02._DTOs;

namespace Simplexity.Traffic.UI.RIA.Web.Services
{
    [ServiceContract(Namespace = "")]
    [SilverlightFaultBehavior]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class TrafficRIAServices
    {
        
        [OperationContract]
        public List<TrafficLoadDTO> GetTrafficLoads()
        {
            return new TrafficServices().GetTrafficLoads();
        }

        [OperationContract]
        public List<EventTypeDTO> GetEventsTypes()
        {
            return new TrafficServices().GetEventTypes();
        }

        
        [OperationContract]
        public List<CheckPointDTO> GetCheckpointsbyLoad(string loanumber)
        {
            return new TrafficServices().GetCheckpointsbyLoad(loanumber);
        }

        [OperationContract]
        public List<EventDTO> GetEventsbyLoad(string loanumber)
        {
            return new TrafficServices().GetEventsbyLoad(loanumber);
        }

        [OperationContract]
        public ResponseDTO CheckPointSetRealDate(CheckPointDTO checkpoint)
        {
            return new TrafficServices().CheckPoint_SetRealDate(checkpoint);
        }

        [OperationContract]
        public ResponseDTO CheckPointCleanRealDate(CheckPointDTO checkpoint)
        {
            return new TrafficServices().CheckPoint_EraseRealDate(checkpoint);
        }

        [OperationContract]
        public ResponseDTO SetLoadEvent(EventDTO loadEvent)
        {
            return new TrafficServices().Event_Register(loadEvent);
        }

        [OperationContract]
        public List<VehiclesGpsDto> GetVehiclesInfo()
        {
            return new TrafficServices().GetVehiclesInfo();
        }

        [OperationContract]
        public string GetUsrcode(string session)
        {
            return new TrafficServices().GetUsrcode(session);
        }
    }
}
