﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using DestinoSeguro;
using System.ServiceModel.Channels;
using DestinoSeguro.ServicioDestinoSeguro;
using DestinoSeguro.Settings;



namespace test
{
  class Program
  {
    static void Main(string[] args)
    {

      //var client = new ServicioDestinoSeguro.WSdestinoPortTypeClient();
      //var result = client.getConsultarVehiculo("WSPRBS", "040915DSdemo", "VN-500002709", "SKV595", "811023952");


      //CustomBinding binding = new CustomBinding();

      //// Add the SymmetricSecurityBindingElement to the BindingElementCollection.
      //binding.Elements.Add(new CustomTextMessageBindingElement("ISO-8859-1", "text/xml", MessageVersion.Soap11));
      //binding.Elements.Add(new HttpTransportBindingElement());
      ////return new CustomBinding(binding);


      var cliente = new Cliente(new Uri(App.DestinoSeguroConfiguration.Address), App.DestinoSeguroConfiguration.User, App.DestinoSeguroConfiguration.Password);


      cliente.ObtenerEventos("VN-500002663", "RRR555", "811023952");


      //var viaje = new ViajeDTO
      //{
      //  FechaSalida = new DateTime(2016 - 03 - 07),
      //  NitTransportador = "1000005",
      //  Manifiesto = "VN-500002700",
      //  CodigoOrigen = "5002000",
      //  CodigoDestino = "76869000",
      //  CodigoVia = "3",
      //  CodigoRuta = "5002000-76869000-3",
      //  Placa = "SBV466",
      //  CodigoMarca = "9",
      //  CodigoCarroceria = "2",
      //  CodigoColor = "8",
      //  Modelo = "2006",
      //  CedulaConductor = "1000424",
      //  NombresConductor = "LEASING DEL VALLE S.A.",
      //  ApellidosConductor = "LEASING DEL VALLE S.A.",
      //  Direccion = "Cll 70 A  4-42",
      //  Telefono = "1 322 0322",
      //  Celular = " 322 0322",
      //  NitGenerador = "888255574",
      //  NombreGenerador = "PruebasSPX",
      //  Sucursal = "JBOG",
      //  Observaciones = "prueba spx"
      //};
      //var crearViajes = new Cliente().ReportarViaje(viaje);

    }
  }
}
