﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Telerik.Windows.Controls.Map;

namespace Simplexity.Traffic.UI.UserControls
{
    public class LineaRuta : Telerik.Windows.Controls.Map.MapPolyline
    {
        public string Placa { get; set; }
    }
    public class Vehiculo
    {

        public Grid Information { get; set; }
        public double RumboGps { get; set; }
        public double VelocidadGps { get; set; }
        public string Estado { get; set; }
        /// <summary>
        /// punto de localizacion GPS
        /// </summary>
        public Location PuntoGps { get; set; }

        /// <summary>
        /// Si hay una busqueda sobre el vehiculo, ademas de centrar aumenta el zoom
        /// </summary>
        public bool AutoZoom { get; set; }

        /// <summary>
        /// Fecha que corresponde al la toma
        /// del punto gps
        /// </summary>
        public string FechaGps { get; set; }

        /// <summary>
        /// Lugar geografico al cual corresponde el 
        /// punto de gps
        /// </summary>
        public string DescrpcionGeoReferenciacion { get; set; }

        /// <summary>
        /// Etiqueta que representa el estado
        /// del vehiculo
        /// </summary>
        public string DescripcionEstado { get; set; }

        /// <summary>
        /// Color que identifica el estado
        /// Este se representará en el bullet de placa
        /// </summary>
        public Color ColorEstado { get; set; }

        /// <summary>
        /// informacion complementaria del vehiculo
        /// para su representacion
        /// de ser formateado.
        /// </summary>
        public string DescrpcionContenido { get; set; }

        /// <summary>
        /// Placa del vehiculo asociado
        /// </summary>
        public string Placa { get; set; }

        /// <summary>
        /// Informacion adicional acerca del Apartado de Identificaion
        /// del Vehiculo
        /// </summary>
        public Location InfoIdentificacion { get; set; }

        /// <summary>
        /// Si es True el vehiculo cada vez que se dibuje el mapa
        /// netrara la posicion correspondiente
        /// </summary>
        public bool MantenerCentrado { get; set; }

        /// <summary>
        /// Visibilidad del vehiculo en la catografia
        /// </summary>
        private bool _visible;

        public MapPinPoint Punto { get; set; }
        public Border Tag { get; set; }

        public bool Visible
        {
            get
            {
                return _visible;
            }
            set
            {
                if (value == true)
                {
                    Punto.Visibility = Visibility.Visible;
                    Tag.Visibility = Visibility.Visible;
                    MapLayer.SetZoomRange(Punto, new ZoomRange(15, 22));
                }
                else
                {
                    Punto.Visibility = Visibility.Collapsed;
                    Tag.Visibility = Visibility.Collapsed;
                    MapLayer.SetZoomRange(Punto, new ZoomRange(50, 60));
                }
                _visible = value;
            }
        }

        public void UbicaVeh(double lat,double lon)
        {
            
            PuntoGps = new Location(lat, lon);
            MapLayer.SetLocation(Punto,PuntoGps);
            MapLayer.SetLocation(Tag, PuntoGps);
        }

        /// <summary>
        /// Inicio de parametros de la clase
        /// </summary>
        public Vehiculo()
        {

        }

    }
}
