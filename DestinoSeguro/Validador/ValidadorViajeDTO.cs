﻿using DestinoSeguro.Dtos;
using FluentValidation;

namespace DestinoSeguro.Validador
{
  public class ValidadorViajeDTO : AbstractValidator<ViajeDTO>
    {
    public ValidadorViajeDTO()
    {
      EstablecerReglas();
    }

    public void EstablecerReglas()
    {
      RuleFor(p => p.FechaSalida).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("FechaSalida");
      RuleFor(p => p.NitTransportador).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("NitTransportador");
      RuleFor(p => p.Manifiesto).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("Manifiesto");
      RuleFor(p => p.CodigoOrigen).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("CodigoOrigen");
      RuleFor(p => p.CodigoDestino).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("CodigoDestino");
      RuleFor(p => p.CodigoVia).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("CodigoVia");
      RuleFor(p => p.Placa).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("Placa");
      RuleFor(p => p.CedulaConductor).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("CedulaConductor");
      RuleFor(p => p.NombresConductor).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("NombresConductor");
      RuleFor(p => p.ApellidosConductor).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("ApelidosConductor");
      RuleFor(p => p.NitGenerador).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("NitGenerador");
      RuleFor(p => p.NombreGenerador).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("NombreGenerador");
      RuleFor(p => p.Sucursal).Cascade(CascadeMode.StopOnFirstFailure).NotNull().OverridePropertyName("Sucursal");

    }
  }
}
