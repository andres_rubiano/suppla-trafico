﻿namespace DestinoSeguro.Settings
{
  public class App
  {
    // static property on any class
    public static DestinoSeguroConfiguration DestinoSeguroConfiguration { get; set; }

    // static constructor ensures this code runs only once 
    // the first time any static property is accessed
    static App()
    {
      /// Load the properties from the Config store
      DestinoSeguroConfiguration = new DestinoSeguroConfiguration();
      DestinoSeguroConfiguration.Initialize();
    }
  }
}
