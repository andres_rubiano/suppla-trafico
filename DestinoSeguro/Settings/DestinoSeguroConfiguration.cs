﻿using Westwind.Utilities.Configuration;

namespace DestinoSeguro.Settings
{
  public class DestinoSeguroConfiguration : AppConfiguration
  {
    public string User { get; set; }
    public string Password { get; set; }
    public string Address { get; set; }
    public string Nittransportadora { get; set; }

    public DestinoSeguroConfiguration()
    {
      User = "WSPRBS";
      Password = "040915DSdemo";
      Address = "http://www.destinoseguro.net/scl/wservices/ws/index.php";
      
    }
  }
}
