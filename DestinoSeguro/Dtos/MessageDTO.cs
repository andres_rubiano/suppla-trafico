﻿using System.Runtime.Serialization;

namespace DestinoSeguro.Dtos
{
  public class MessageDTO
  {
    private MessagesConstants.Types _typeEnum;

    public MessageDTO()
    {
      _typeEnum = MessagesConstants.Types.Information;
    }

    [IgnoreDataMember]
    public MessagesConstants.Types TypeEnum
    {
      set { _typeEnum = value; }
      get { return _typeEnum; }
    }

    [DataMember]
    public int Type
    {
      get { return (int)_typeEnum; }
      set { }
    }

    [DataMember]
    public string TransactionNumber { get; set; }

    [DataMember]
    public string Message { get; set; }

    public class MessagesConstants
    {
      public enum Types
      {
        None = 0,
        Information = 1,
        Warning = 2,
        Error = 3
      }
    }
  }
}
