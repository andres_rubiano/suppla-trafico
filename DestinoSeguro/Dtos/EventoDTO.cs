﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DestinoSeguro.Dtos
{
  public class EventoDTO
  {
    public int IdPuestoControl { get; set; }
    public string TipoEvento { get; set; }
    public string NumeroViaje { get; set; }
    public string Placa { get; set; }
    public string CodigoEvento { get; set; }
    public string NombreEvento { get; set; }
    public DateTime FechaEvento { get; set; }
    public string Observaciones { get; set; }
    public string Usuario { get; set; }
  }
}
