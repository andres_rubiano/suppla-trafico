﻿using System.Collections.Generic;

namespace DestinoSeguro.Dtos
{
  public class RespuestaEventosDTO
  {
    public RespuestaEventosDTO()
    {
      MessageDto = new MessageDTO();
      Eventos = new List<EventoDTO>();
    }

    public MessageDTO MessageDto { get; set; }
    public List<EventoDTO> Eventos { get; set; }
  }
}
