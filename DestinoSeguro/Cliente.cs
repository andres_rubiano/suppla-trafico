﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using DestinoSeguro.Dtos;
using DestinoSeguro.ServicioDestinoSeguro;
using DestinoSeguro.Validador;
using FluentValidation;
using Microsoft.Samples.CustomTextMessageEncoder;


namespace DestinoSeguro
{
    public class Cliente
    {
      private WSdestinoPortTypeClient destinoSeguro;
      private string _user;
      private readonly string _password;

      public Cliente( Uri uri, string user, string password)
      {
        _user = user;
        _password = password;

        CustomBinding binding = new CustomBinding();

        // Add the SymmetricSecurityBindingElement to the BindingElementCollection.
        binding.Elements.Add(new CustomTextMessageBindingElement("ISO-8859-1", "text/xml", MessageVersion.Soap11));
        binding.Elements.Add(new HttpTransportBindingElement());
        //return new CustomBinding(binding);


        destinoSeguro = new WSdestinoPortTypeClient(binding, new EndpointAddress(uri));
      }

      public MessageDTO ReportarViaje(ViajeDTO viajeDTO)
      {
        var messageDto = new MessageDTO();
        
        //1. Validar el mensaje

        var validacion = new ValidadorViajeDTO().Validate(viajeDTO);

        if (!validacion.IsValid)
        {
          foreach (var failure in validacion.Errors)
          {
            var error = string.Join("|", failure.ErrorMessage);
            messageDto.TypeEnum = MessageDTO.MessagesConstants.Types.Error;
            messageDto.Message = error;
          }
          return  messageDto; 
        }

        var resultado = destinoSeguro.setDespachoTransp(_user,
                                                        _password,
                                                        viajeDTO.FechaSalida.ToString("yyyy-MM-ddTHH:mm:ss"),
                                                        viajeDTO.NitTransportador,
                                                        viajeDTO.Manifiesto,
                                                        viajeDTO.CodigoOrigen,
                                                        viajeDTO.CodigoDestino,
                                                        viajeDTO.CodigoVia,
                                                        viajeDTO.CodigoRuta,
                                                        viajeDTO.Placa,
                                                        viajeDTO.CodigoMarca,
                                                        viajeDTO.CodigoCarroceria,
                                                        viajeDTO.CodigoColor,
                                                        viajeDTO.Modelo,
                                                        viajeDTO.CedulaConductor,
                                                        viajeDTO.NombresConductor,
                                                        viajeDTO.ApellidosConductor,
                                                        viajeDTO.Direccion,
                                                        viajeDTO.Telefono,
                                                        viajeDTO.Celular,
                                                        viajeDTO.NitGenerador,
                                                        viajeDTO.NombreGenerador,
                                                        viajeDTO.Sucursal,
                                                        viajeDTO.Observaciones,"","","",
                                                        "","","", new generador(), "");
        var viajeRespuesta = resultado.Split('|');
        int indicadorError;
        if (int.TryParse(viajeRespuesta[0], out indicadorError) && indicadorError == 0)
        {
          messageDto.Message = viajeRespuesta[1];
          messageDto.TypeEnum = MessageDTO.MessagesConstants.Types.Error;
        }
        if (int.TryParse(viajeRespuesta[0], out indicadorError) && indicadorError == 1)
        {
          messageDto.Message = viajeRespuesta[1];
          messageDto.TypeEnum = MessageDTO.MessagesConstants.Types.Information;
        }
        return messageDto;
      }

      public RespuestaEventosDTO ObtenerEventos(string numeroViaje, string placa, string nitTransportador)
      {
        var respuestaEventosDTO = new RespuestaEventosDTO();

        //validar datos de entrada
        if (numeroViaje== null || placa==null || nitTransportador== null)
        {
          respuestaEventosDTO.MessageDto.TypeEnum= MessageDTO.MessagesConstants.Types.Error;
          respuestaEventosDTO.MessageDto.Message = "Campos incompletos";
          return respuestaEventosDTO;
        }

        var resultado = destinoSeguro.getConsultarVehiculo(_user, _password, nitTransportador, placa, numeroViaje);
        
        //Se realiza split por ; cuando el mensaje es exitoso por la estructura que retorna
        var eventoRespuesta = resultado.Split(';');
        
        var eventos = new List<EventoDTO>();
        if (eventoRespuesta.Length > 1)
        {
          foreach (var s in eventoRespuesta)
          {
            var eventoDTO = new EventoDTO();
            //Se realiza este split para generar un objeto de tipo eventoDTO
            var serie = s.Split(',');
            if ( serie.Length > 1)
            {
              if (serie[1] == "P/C")
              {
                eventoDTO.IdPuestoControl = Convert.ToInt32(serie[4]);
                eventoDTO.TipoEvento = serie[1];
                eventoDTO.NumeroViaje = serie[2];
                eventoDTO.Placa = serie[3];
                eventoDTO.NombreEvento = serie[5];
                eventoDTO.FechaEvento = (DateTime.Parse(serie[8]));
                eventoDTO.Observaciones = serie[7];
                eventos.Add(eventoDTO);
              }
              else
              {
                eventoDTO.IdPuestoControl = Convert.ToInt32(serie[0]);
                eventoDTO.TipoEvento = serie[1];
                eventoDTO.NumeroViaje = serie[2];
                eventoDTO.Placa = serie[3];
                eventoDTO.CodigoEvento = serie[4];
                eventoDTO.NombreEvento = serie[5];
                eventoDTO.FechaEvento = (DateTime.Parse(serie[6]));
                eventoDTO.Observaciones = serie[7];
                eventoDTO.Usuario = serie[8];
                eventos.Add(eventoDTO);
              }
              
            }
          }
          respuestaEventosDTO.Eventos = eventos;
        }
        else
        {
          //este split se hace para capturar el mensaje cuando no hay eventos para este viaje por que viene con dos 0 separados por un - 
          var mensaje = resultado.Split('-');
          if (mensaje.Length == 3)
          {
            respuestaEventosDTO.MessageDto.TypeEnum = MessageDTO.MessagesConstants.Types.Information;
            respuestaEventosDTO.MessageDto.Message = mensaje[2];
          }
          else
          {
            respuestaEventosDTO.MessageDto.TypeEnum = MessageDTO.MessagesConstants.Types.Error;
            respuestaEventosDTO.MessageDto.Message = "Error de autenticacion";
          }
        }
        return respuestaEventosDTO;
      }
    }
}
