﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Simplexity.Traffic.UI.RIA.Util
  {
    public class ColorCodes : IValueConverter
      {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
          {
            if (value != null)
              {
                if ((double) value <= 0)
                  {
                    return "Images/Cal_Fond.png"; // new SolidColorBrush(Color.FromArgb(250, 90, 90, 90)); 
                  }
                if ((double) value > 0 && ((double) value <= 60))
                  {
                    return "Images/Cal_yellow.png"; //new SolidColorBrush(Color.FromArgb(248, 248, 232, 23));
                  }
                if ((double) value > 60 && (double) value <= 120)
                  {
                    return "Images/Cal_orange.png"; //new SolidColorBrush(Color.FromArgb(248, 248, 145, 23));
                  }
                if ((double) value >= 120)
                  {
                    return "Images/Cal_red.png"; //new SolidColorBrush(Color.FromArgb(248, 248, 23, 23));
                  }
              }
            return value.ToString();
          }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
          {
            throw new NotImplementedException();
          }

        #endregion
      }
  }