﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel.Channels;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Threading;
using AutoMapper;
using Simplexity.Traffic.UI.RIA.ServiceReference;
using Simplexity.Traffic.UI.RIA.TrafficServicesReference;
using Telerik.Windows;
using Telerik.Windows.Controls;
using EventDTO = Simplexity.Traffic.UI.RIA.ServiceReference.EventDTO;
using MessagesConstantsTypes = Simplexity.Traffic.UI.RIA.ServiceReference.MessagesConstantsTypes;
using TrafficLoadDTO = Simplexity.Traffic.UI.RIA.ServiceReference.TrafficLoadDTO;

namespace Simplexity.Traffic.UI.RIA
  {
    public partial class MainPage : Page
      {
        public MainPage()
          {
            InitializeComponent();
            if (!DesignerProperties.GetIsInDesignMode(this))
              {
                LoadLoads();
                Temporizador();
              }
          }

        private void Temporizador()
          {
            var timer = new DispatcherTimer();
            Timer.SelectionChanged +=
              delegate
                {
                  timer.Stop();
                  int minute;
                  switch (Timer.SelectionBoxItem.ToString())
                    {
                      case "1 Minuto":
                        minute = 1;
                        break;
                      case "5 Minutos":
                        minute = 5;
                        break;
                      case "10 Minutos":
                        minute = 10;
                        break;
                      default:
                        minute = 0;
                        break;
                    }
                  timer.Interval = new TimeSpan(0, minute, 0); // one second
                  timer.Start();
                };
            timer.Tick +=
              delegate { LoadLoads(); };
            int min;
            switch (Timer.SelectionBoxItem.ToString())
              {
                case "1 Minuto":
                  min = 1;
                  break;
                case "5 Minutos":
                  min = 5;
                  break;
                case "10 Minutos":
                  min = 10;
                  break;
                default:
                  min = 0;
                  break;
              }
            timer.Interval = new TimeSpan(0, min, 0); // one second
            timer.Start();
          }

        private void LoadLoads()
          {
            var client = new TrafficRIAServicesClient();

            client.GetTrafficLoadsAsync();
            client.GetTrafficLoadsCompleted += ClientGetTrafficLoadsCompleted;
          }

        private void LoadCheckpointsEvents()
          {
            if (Loads.SelectedItem == null) return;

            var loanumber = ((TrafficLoadDTO) Loads.SelectedItem).LoadNumber;
            //var loanumber = "0";
            var client = new TrafficRIAServicesClient();

            client.GetCheckpointsbyLoadAsync(loanumber);
            client.GetCheckpointsbyLoadCompleted += ClientOnGetCheckpointsbyLoadCompleted;

            client.GetEventsbyLoadAsync(loanumber);
            client.GetEventsbyLoadCompleted += ClientOnGetEventsbyLoadCompleted;
          }

        private void ClientGetTrafficLoadsCompleted(object sender, GetTrafficLoadsCompletedEventArgs e)
          {
            if (e.Result.Count > 0)

              Loads.ItemsSource = e.Result;
            else
              {
                //    //RadWindow.Alert(new DialogParameters
                //    //{
                //    //    Header = "Alerta",
                //    //    Content = Msg.Warning_NoLoads,
                //    //    Theme = new Windows8Theme()
                //    //}); 
              }
          }

        private void LoadsSelectionChanged(object sender, SelectionChangeEventArgs e)
          {
            LoadCheckpointsEvents();
          }

        private void ClientOnGetCheckpointsbyLoadCompleted(object sender, GetCheckpointsbyLoadCompletedEventArgs e)
          {
            if (e.Result.Count > 0)
              Checkpoints.ItemsSource = e.Result;
            else
              {
                //RadWindow.Alert(new DialogParameters
                //{
                //    Header = "Alerta",
                //    Content = string.Format(Msg.Warning_NoCheckpoints,(((TrafficLoadDTO)Loads.SelectedItem).LoadNumber)),
                //    Theme = new Windows8Theme()
                //});
                Checkpoints.ItemsSource = null;
              }
          }

        private void ClientOnGetEventsbyLoadCompleted(object sender, GetEventsbyLoadCompletedEventArgs e)
          {
            if (e.Result.Count > 0)
              {
                Mapper.CreateMap<EventDTO, EventModel>()
                      .ForMember(c => c.TfeSource, c => c.MapFrom(g => g.TfeSource == 1 ? "Torre De Control" : "Destino Seguro"));
                Events.ItemsSource = Mapper.Map<IEnumerable<EventDTO>, IEnumerable<EventModel>>(e.Result);
              }
            else
              {
                //RadWindow.Alert(new DialogParameters
                //{
                //    Header = "Alerta",
                //    Content = string.Format(Msg.Warning_NoEvents, (((TrafficLoadDTO)Loads.SelectedItem).LoadNumber)),
                //    Theme = new Windows8Theme()
                //});
                Events.ItemsSource = null;
              }
          }

        private void SetRealDateClick(object sender, Telerik.Windows.RadRoutedEventArgs e)
          {
            if (Checkpoints.SelectedItem == null) return;

            var checkpoint = (CheckPointDTO) Checkpoints.SelectedItem;
            checkpoint.RealDate = DateTime.Now;

            var client = new TrafficRIAServicesClient();
            client.CheckPointSetRealDateAsync(checkpoint);
            client.CheckPointSetRealDateCompleted += ClientOnCheckPointSetRealDateCompleted;
          }

        private void ClientOnCheckPointSetRealDateCompleted(object sender, CheckPointSetRealDateCompletedEventArgs e)
          {
            if (e.Result != null)
              {
                if (e.Result.Message.TypeEnum == MessagesConstantsTypes.Information)
                  {
                    LoadCheckpointsEvents();
                  }
                else
                  {
                    //RadWindow.Alert(new DialogParameters
                    //{
                    //    Header = "Alerta",
                    //    Content = e.Result.Message.Message,
                    //    Theme = new Windows8Theme()
                    //});
                  }
              }
          }

        private void EraseRealDateClick(object sender, Telerik.Windows.RadRoutedEventArgs e)
          {
            if (Checkpoints.SelectedItem == null) return;

            var checkpoint = (CheckPointDTO) Checkpoints.SelectedItem;
            checkpoint.RealDate = DateTime.Now;

            var client = new TrafficRIAServicesClient();
            client.CheckPointCleanRealDateAsync(checkpoint);
            client.CheckPointCleanRealDateCompleted += ClientOnCheckPointCleanRealDateCompleted;
          }

        private void ClientOnCheckPointCleanRealDateCompleted(object sender, CheckPointCleanRealDateCompletedEventArgs e)
          {
            if (e.Result != null)
              {
                if (e.Result.Message.TypeEnum == MessagesConstantsTypes.Information)
                  {
                    LoadCheckpointsEvents();
                  }
                else
                  {
                    //RadWindow.Alert(new DialogParameters
                    //{
                    //    Header = "Alerta",
                    //    Content = e.Result.Message.Message,
                    //    Theme = new Windows8Theme()
                    //});
                  }
              }
          }

        private void AddEventClick(object sender, Telerik.Windows.RadRoutedEventArgs e)
          {
            if (Loads.SelectedItem == null) return;
            var load = (TrafficLoadDTO) Loads.SelectedItem;
            var addEvent = new AddEvent(load.LoadNumber);
            addEvent.ShowDialog();
          }

        private void RadButton_Click(object sender, RoutedEventArgs e)
          {
            //if (Loads.SelectedItem == null) return;
            //this.NavigationService.Navigate(new Uri("/Mapas.xaml", UriKind.Relative));
            var mainPage = Application.Current.RootVisual as MainPage;
            if (mainPage != null)
              ((Frame) mainPage.FindName("ContentFrame")).Navigate(new Uri("/Mapas.xaml", UriKind.Relative));
          }

        private void RadButton_Click_1(object sender, RoutedEventArgs e)
          {
            try
              {
                const string extension = "xls";
                const ExportFormat format = ExportFormat.ExcelML;

                var dialog = new SaveFileDialog
                               {
                                 DefaultExt = extension,
                                 Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                                 FilterIndex = 1
                               };

                if (dialog.ShowDialog() == true)
                  {
                    using (var stream = dialog.OpenFile())
                      {
                        var exportOptions = new GridViewExportOptions
                                              {Format = format, ShowColumnFooters = true, ShowColumnHeaders = true, ShowGroupFooters = true};

                        Loads.Export(stream, exportOptions);
                      }
                  }
              }
            catch (Exception ex)
              {
                RadWindow.Alert(new DialogParameters
                                  {
                                    Header = "Alerta",
                                    Content = ex.Message,
                                    Theme = new Windows8Theme()
                                  });
              }
          }

        private void RadButton_Click_2(object sender, RoutedEventArgs e)
          {
            LoadLoads();
            Temporizador();
          }

        private void ClientOnGetDestinoSeguroEventsbyLoadCompleted(object sender, GetDestinoSeguroEventCompletedEventArgs e)
          {
            if (e.Result.GetDestinoSeguroEventResult.Eventos.Length > 0)
              {
                //Events.ItemsSource = e.Result.GetDestinoSeguroEventResult;
                LoadCheckpointsEvents();
              }
            else
              {
                //muestra una pantalla con el mensaje que retorna destino seguro cuando hay alguna clase de error 
                RadWindow.Alert(new DialogParameters
                                  {
                                    Header = "Informacion Destino Seguro",
                                    Content = e.Result.GetDestinoSeguroEventResult.MessageDto.Message,
                                    Theme = new Windows8Theme()
                                  });
                Events.ItemsSource = null;
              }
          }

        private void ObtenerDS_OnClick(object sender, RadRoutedEventArgs e)
          {
            if (Loads.SelectedItem == null) return;

            var loanumber = ((TrafficLoadDTO) Loads.SelectedItem).LoadNumber;
            var vehicleCode = ((TrafficLoadDTO) Loads.SelectedItem).VehicleCode;
            string usrCode = ((App) Application.Current).UniqueUsrCode;

            //var loanumber = "0";

            var clienteRia = new TrafficRIAServicesClient();

            clienteRia.GetUsrcodeAsync(usrCode);
            clienteRia.GetUsrcodeCompleted += (o, args) =>
                                                {
                                                  var cliente = new TrafficServicesClient();

                                                  cliente.GetDestinoSeguroEventAsync(new GetDestinoSeguroEventRequest(loanumber, vehicleCode, args.Result));
                                                  cliente.GetDestinoSeguroEventCompleted += ClientOnGetDestinoSeguroEventsbyLoadCompleted;
                                                };
          
          }

        private void GetLoadInfo_OnClick(object sender, RadRoutedEventArgs e)
          {
            var loanumber = ((TrafficLoadDTO)Loads.SelectedItem).LoadNumber;
            App application = (App) Application.Current;
            var urlLoadInfo = new Uri(application.UrlSiteTc + "/TC/Traffic/lstTfInfoLoad_ctm.asp?ID=" + loanumber, UriKind.Absolute);
            HtmlPage.Window.Navigate(urlLoadInfo, "lstTfInfoLoad_ctm", "height=500,width=800,top=100,left=100");
          }

      }
      }
  