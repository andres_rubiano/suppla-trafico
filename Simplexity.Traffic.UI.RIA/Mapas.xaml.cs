﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Simplexity.Traffic.UI.RIA.ServiceReference;
using Simplexity.Traffic.UI.UserControls;

namespace Simplexity.Traffic.UI.RIA
{
    public partial class Mapas
    {
        public string Sucursal;
        public string Empresa;
        public string Cliente;
        public DispatcherTimer MyDispatcherTimer = new DispatcherTimer();

        public Mapas()
        {
            InitializeComponent();
            simplexityVehicleMap1.M00StartAppMap();
            CargarUnidadesV2();
            MyDispatcherTimer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, 30000) };
            MyDispatcherTimer.Tick += Cargar;
            MyDispatcherTimer.Start();
        }

        public void Cargar(object s, EventArgs e)
        {
            MyDispatcherTimer.Stop();
            CargarUnidadesV2();
            MyDispatcherTimer.Start();
        }

        public void MapasUnloaded(object sender, RoutedEventArgs e)
        {
        }

        public void CargarUnidadesV2()
        {
            //var wsc = new TrafficReference.TrafficRIAServicesClient();
            var wsc = new TrafficRIAServicesClient();
            wsc.GetVehiclesInfoAsync();
            wsc.GetVehiclesInfoCompleted += WscOnGetVehiclesInfoCompleted;
        }

        private void WscOnGetVehiclesInfoCompleted(object sender, GetVehiclesInfoCompletedEventArgs e)
        {
            var t1 = e.Result.ToList();
            var j1 = t1;
            lstVehiculos.ItemsSource = j1.OrderBy(x => x.VehCode);
            lstVehiculos.DisplayMemberPath = "VehCode";

            foreach (var w in j1)
            {
                if (w.GeoRef!=null)
                {
                    var strTexto = "\nReferencia:" + w.GeoRef.Replace("|", "\n");
                    if (w.Deltac > 0 && w.Deltac < 30)
                    {
                        simplexityVehicleMap1.M02AddVehicle(w.VehCode, w.Latitude, w.Longitude,
                                                            Color.FromArgb(248, 248, 232, 23), w.Status,
                                                            strTexto,
                                                            SimplexityVehicleMap.Imagenes.CamionNaranja3);

                    }
                    if (w.Deltac >= 30 && w.Deltac < 60)
                    {
                        simplexityVehicleMap1.M02AddVehicle(w.VehCode, w.Latitude, w.Longitude,
                                                            Color.FromArgb(248, 248, 145, 23), w.Status,
                                                            strTexto,
                                                            SimplexityVehicleMap.Imagenes.CamionNaranja3);

                    }
                    if (w.Deltac >= 60 && w.Deltac < 120)
                    {
                        simplexityVehicleMap1.M02AddVehicle(w.VehCode, w.Latitude, w.Longitude,
                                                            Color.FromArgb(248, 248, 23, 23), w.Status,
                                                            strTexto,
                                                            SimplexityVehicleMap.Imagenes.CamionNaranja3);

                    }
                    if (w.Deltac >= 120)
                    {
                        simplexityVehicleMap1.M02AddVehicle(w.VehCode, w.Latitude, w.Longitude,
                                                            Color.FromArgb(248, 65, 6, 47), w.Status,
                                                            strTexto,
                                                            SimplexityVehicleMap.Imagenes.CamionNaranja3);

                    }
                    if (w.Deltac < 0)
                    {
                        simplexityVehicleMap1.M02AddVehicle(w.VehCode, w.Latitude, w.Longitude,
                                                            Color.FromArgb(250, 90, 90, 90), w.Status,
                                                            strTexto,
                                                            SimplexityVehicleMap.Imagenes.CamionNaranja3);
                    }
                }
            }

            DispatcherOperation dispatcherOperation1 =
            label1a.Dispatcher.BeginInvoke(delegate()
            {
                label1a.Content = "Actualización\n" + DateTime.Now.ToString(CultureInfo.InvariantCulture);
            });

            var estado = "";
            var cbi = comboBox1.SelectedItem as ComboBoxItem;
            estado = (cbi == null ? "Todos" : cbi.Content.ToString()) ?? "Todos";

            simplexityVehicleMap1.M09CentrarVehiculos(estado);

        }

        private void txtVehiculo_TextChanged(object sender, TextChangedEventArgs e)
        {
            simplexityVehicleMap1.M06CentrarVehiculo(txtVehiculo.Text.ToUpper());
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            if (lstVehiculos.SelectedIndex >= 0)
            {
                var tmp = (VehiclesGpsDto)lstVehiculos.SelectedItem;
                simplexityVehicleMap1.M06CentrarVehiculoSolo(tmp.VehCode);
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                checkBox2.IsChecked = q.MantenerCentrado;
                checkBox4.IsChecked = q.Visible;
                checkBox3.IsChecked = q.AutoZoom;
            }
        }

        private void checkBox2_Checked_1(object sender, RoutedEventArgs e)
        {
            //mantener centrado
            if (lstVehiculos.SelectedIndex >= 0)
            {
                var tmp = (VehiclesGpsDto)lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.MantenerCentrado = true;

            }
        }

        private void checkBox2_Unchecked(object sender, RoutedEventArgs e)
        {
            //mantener centrado
            if (lstVehiculos.SelectedIndex >= 0)
            {
                var tmp = (VehiclesGpsDto)lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.MantenerCentrado = false;

            }
        }

        private void checkBox4_Checked(object sender, RoutedEventArgs e)
        {
            //mantener visible
            if (lstVehiculos.SelectedIndex >= 0)
            {
                var tmp = (VehiclesGpsDto)lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.Visible = true;

            }
        }

        private void checkBox4_Unchecked(object sender, RoutedEventArgs e)
        {
            //mantener visible
            if (lstVehiculos.SelectedIndex >= 0)
            {
                var tmp = (VehiclesGpsDto)lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.Visible = false;

            }
        }

        private void checkBox3_Checked(object sender, RoutedEventArgs e)
        {
            //mantener visible
            if (lstVehiculos.SelectedIndex >= 0)
            {
                var tmp = (VehiclesGpsDto)lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.AutoZoom = true;

            }
        }

        private void checkBox3_Unchecked(object sender, RoutedEventArgs e)
        {
            //mantener visible
            if (lstVehiculos.SelectedIndex >= 0)
            {
                var tmp = (VehiclesGpsDto)lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.AutoZoom = false;

            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            simplexityVehicleMap1.M08OcultarFlota(false);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            simplexityVehicleMap1.M08OcultarFlota(true);
        }

        private void comboBox1_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            var estado = ((Telerik.Windows.Controls.RadComboBoxItem)(comboBox1.SelectedItem)).Content.ToString() ?? "Todos";
            simplexityVehicleMap1.M09CentrarVehiculos(estado);
        }

        private void LstVehiculosSelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (lstVehiculos.SelectedItems.Count >= 0)
            {
                var tmp = (VehiclesGpsDto)lstVehiculos.SelectedItem;
                if(tmp==null) return;
                simplexityVehicleMap1.M06CentrarVehiculo(tmp.VehCode);
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).FirstOrDefault();
                if (q == null) return;
                //es solo un resultado
                checkBox2.IsChecked = q.MantenerCentrado;
                checkBox4.IsChecked = q.Visible;
                checkBox3.IsChecked = q.AutoZoom;
            }
        }

        private void RadButton_Click(object sender, RoutedEventArgs e)
        {
            var mainPage = Application.Current.RootVisual as MainPage;
            if (mainPage != null)
                ((Frame)mainPage.FindName("ContentFrame")).Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }

    }
}
