﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Simplexity.Traffic.UI.RIA.ServiceReference;
using Telerik.Windows.Controls;

namespace Simplexity.Traffic.UI.RIA
{
    /// <summary>
    /// Interaction logic for AddEvent.xaml
    /// </summary>
    public partial class AddEvent
    {
        public DateTime DateEvent { get; set; }

        public AddEvent(string load)
        {
            InitializeComponent();
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                var client = new TrafficRIAServicesClient();

                client.GetEventsTypesAsync();
                client.GetEventsTypesCompleted += ClientOnGetEventsTypesCompleted;

                client.GetCheckpointsbyLoadAsync(load);
                client.GetCheckpointsbyLoadCompleted += ClientOnGetCheckpointsbyLoadCompleted;
                
                LoadNumber.Text = load;
                
            }

            DateEvent = DateTime.Now;
        }

        private void ClientOnGetCheckpointsbyLoadCompleted(object sender, GetCheckpointsbyLoadCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                e.Result.Insert(0, new CheckPointDTO{CheckpointTrcCode = "MANUAL"});
                Checkpoints.ItemsSource = e.Result;
            }
            else
            {
                Checkpoints.Visibility = Visibility.Collapsed;
               
                Manual.Visibility = Visibility.Visible;
            }
        }

        private void ClientOnGetEventsTypesCompleted(object sender, GetEventsTypesCompletedEventArgs e)
        {
            if (e.Result.Count > 0)
            {
                EventTypes.ItemsSource = e.Result;
            }
            else
            {
                Close();
            }
        }

        private void RadButtonClick(object sender, RoutedEventArgs e)
        {
          if ((DepartureDateRad.SelectedDate == null || EventTypes.SelectedItem == null || (Checkpoints.SelectedItem == null && Manual.Visibility != Visibility.Visible)) || (Manual.Visibility == Visibility.Visible && string.IsNullOrEmpty(Manual.Text)))
          {
            Alert(new DialogParameters
            {
              Header = "Alerta",
              Content = "Verifique que todos los campos esten digitados correctamente",
              Theme = new Windows8Theme()
              
            });
            return;
          }
         // if (Checkpoints.SelectedIndex==0 && string.IsNullOrEmpty(Manual.Text)) return;

          #region FailCookie
            //// Create the client WebRequest creator.
            //IWebRequestCreate creator = WebRequestCreator.ClientHttp;
            //// Register both http and https.
            //WebRequest.RegisterPrefix("http://", creator);
            //WebRequest.RegisterPrefix("https://", creator);
            //// Create a HttpWebRequest.
            //HttpWebRequest request = (HttpWebRequest)
            //    WebRequest.Create("http://" + Application.Current.Host.Source.Host + ":" + Application.Current.Host.Source.Port + "/clientaccesspolicy.xml");

            //Alert(new DialogParameters
            //{
            //  Header = "Alerta",
            //  Content = "http://" + Application.Current.Host.Source.Host + ":" + Application.Current.Host.Source.Port + "/clientaccesspolicy.xml",
            //  Theme = new Windows8Theme()
            //});
            ////Create the cookie container and add a cookie.
            //request.CookieContainer = new CookieContainer();
            //// Send the request.
            //request.BeginGetResponse(f =>
            //{
            //    var r = (HttpWebRequest)f.AsyncState;
            //    var response = (HttpWebResponse)
            //    r.EndGetResponse(f);
            //    foreach (Cookie cookieValue in response.Cookies)
            //    {
            //        Alert(new DialogParameters
            //        {
            //            Header = "Alerta",
            //            Content = cookieValue.Value,
            //            Theme = new Windows8Theme()
            //        });
            //        string[] keyValuePair = cookieValue.Value.Split('=');
            //        if (keyValuePair.Length == 2 && "session" == keyValuePair[0].Trim().ToLower())
            //        {
            //            usrcode = keyValuePair[1].Trim();
            //            Alert(new DialogParameters
            //            {
            //                Header = "Alerta",
            //                Content = cookieValue.Value + "      " + keyValuePair[0].Trim(),
            //                Theme = new Windows8Theme()
            //            });
            //        }
            //    }
            //},request);
#endregion

            string usrcode = ((App) Application.Current).UniqueUsrCode;
            var client = new TrafficRIAServicesClient();

            client.GetUsrcodeAsync(usrcode);
            client.GetUsrcodeCompleted+=ClientOnGetUsrcodeCompleted;
        }
      
        private void ClientOnGetUsrcodeCompleted(object sender, GetUsrcodeCompletedEventArgs e)
          {
            if (e.Result == null) return;
            string usrcode = e.Result == "" ? "silverlight" : e.Result;
            var client = new TrafficRIAServicesClient();
            client.SetLoadEventAsync(new EventDTO()
                                       {
                                         TfeLoad_LoaNumber = LoadNumber.Text,
                                         TfeCheckpoint_TrcCode = string.IsNullOrEmpty(Manual.Text)
                                           ? ((CheckPointDTO) (Checkpoints.SelectedItem)).CheckpointTrcCode
                                           : Manual.Text,
                                         TfeDate = DepartureDateRad.SelectedValue ?? DateTime.Now,
                                         TfeText = Comment.Text,
                                         TfeType = ((EventTypeDTO) EventTypes.SelectedItem).TetCode,
                                         TfeCreation_UsrCode = usrcode,
                                         TfeSource = 1
                                       });
            client.SetLoadEventCompleted += ClientOnSetLoadEventCompleted;
          }

        private void ClientOnSetLoadEventCompleted(object sender, SetLoadEventCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                if (e.Result.Message.TypeEnum == MessagesConstantsTypes.Information)
                {
                    Close();
                }
                else
                {
                    Alert(new DialogParameters
                    {
                        Header = "Alerta",
                        Content = e.Result.Message.Message,
                        Theme = new Windows8Theme()
                    });
                }
                var mainPage = Application.Current.RootVisual as MainPage;
                if (mainPage != null)
                    ((Frame)mainPage.FindName("ContentFrame")).Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
        }

        private void RadButtonClick1(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Checkpoints_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (!((CheckPointDTO) (Checkpoints.SelectedItem)).CheckpointTrcCode.Equals("MANUAL")) return;

            Checkpoints.Visibility = Visibility.Collapsed;
            
            Manual.Visibility = Visibility.Visible;
        }

        private void DepartureDate_OnKeyUp(object sender, KeyEventArgs e)
        {
            // e.PlatformKeyCode = 190 para ":"   
            if (e.PlatformKeyCode == 190)
            {
                DepartureDateRad.SelectedValue = DateTime.Now;
            }
            
        }
    }
}
