﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Simplexity.Traffic.UI.RIA
  {
    public partial class App : Application
      {
        public IDictionary<string, string> DeploymentConfigurations;

        private string _uniqueUsrCode;
        private string _urlSiteTc;

        public string UniqueUsrCode
          {
            get { return _uniqueUsrCode; }
          }

        public string UrlSiteTc
          {
            get { return _urlSiteTc; }
          }

        public App()
          {
            this.Startup += this.Application_Startup;
            this.Exit += this.Application_Exit;
            this.UnhandledException += this.Application_UnhandledException;

            Windows8Palette.Palette.AccentColor = Color.FromArgb(0xFF, 0x4C, 0x71, 0xA5); //4C71A5
            Windows8Palette.Palette.FontSize = 11;
            Windows8Palette.Palette.FontSizeXS = 9;
            Windows8Palette.Palette.FontSizeS = 10;
            Windows8Palette.Palette.FontSizeL = 12;
            Windows8Palette.Palette.FontSizeXL = 14;
            Windows8Palette.Palette.FontSizeXXL = 16;
            Windows8Palette.Palette.FontSizeXXXL = 22;

            Windows8Palette.Palette.FontFamily = new FontFamily("Calibri");
            Windows8Palette.Palette.FontFamilyLight = new FontFamily("Calibri");
            Windows8Palette.Palette.FontFamilyStrong = new FontFamily("Calibri");


            StyleManager.ApplicationTheme = new Windows8Theme();

            InitializeComponent();
          }

        private void Application_Startup(object sender, StartupEventArgs e)
          {
            //DeploymentConfigurations = e.InitParams;

            Thread.CurrentThread.CurrentCulture = new CultureInfo("es");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("es");
            try
              {
                _uniqueUsrCode = HtmlPage.Document.QueryString["UniqueUsrCode"];
                _urlSiteTc = "http://" + Current.Host.Source.Host + ":" + Current.Host.Source.Port;
              }
            catch (Exception ex)
              {
                HtmlPage.Window.Navigate(new Uri("http://" + Current.Host.Source.Host + ":" + Current.Host.Source.Port, UriKind.RelativeOrAbsolute));
              }
            RootVisual = new MainPage();
          }

        private void Application_Exit(object sender, EventArgs e)
          {
          }

        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
          {
            // If the app is running outside of the debugger then report the exception using
            // the browser's exception mechanism. On IE this will display it a yellow alert 
            // icon in the status bar and Firefox will display a script error.
            if (!System.Diagnostics.Debugger.IsAttached)
              {
                // NOTE: This will allow the application to continue running after an exception has been thrown
                // but not handled. 
                // For production applications this error handling should be replaced with something that will 
                // report the error to the website and stop the application.
                e.Handled = true;
                Deployment.Current.Dispatcher.BeginInvoke(delegate { ReportErrorToDOM(e); });
              }
          }

        private void ReportErrorToDOM(ApplicationUnhandledExceptionEventArgs e)
          {
            try
              {
                string errorMsg = e.ExceptionObject.Message + e.ExceptionObject.StackTrace;
                errorMsg = errorMsg.Replace('"', '\'').Replace("\r\n", @"\n");

                System.Windows.Browser.HtmlPage.Window.Eval("throw new Error(\"Unhandled Error in Silverlight Application " + errorMsg + "\");");
              }
            catch (Exception)
              {
              }
          }
      }
  }