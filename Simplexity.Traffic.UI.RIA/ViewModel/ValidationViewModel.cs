﻿using System;
using System.ComponentModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Simplexity.Traffic.UI.RIA.ViewModel
{
    public class ValidationViewModel : ViewModelBase, IDataErrorInfo
    {
        public ValidationViewModel()
        {
            this._DepartureDateChanged = false;
            this._Reserve = new DelegateCommand(this.ReserveAction);
            DepartureDate = DateTime.Now;
        }

        private ICommand _Reserve;
        public ICommand Reserve
        {
            get
            {
                return this._Reserve;
            }
        }

        private void ReserveAction(object parameter)
        {
            //RadWindow.Alert(new DialogParameters
            //{
            //    Header = "Alerta",
            //    Content = parameter,
            //    Theme = new Windows8Theme()
            //}); 
            this._DepartureDateChanged = true;
            this.OnPropertyChanged("DepartureDate");
        }

        private bool _DepartureDateChanged;

        private DateTime? _DepartureDate;
        public DateTime? DepartureDate
        {
            get
            {
                return this._DepartureDate;
            }
            set
            {
                if (this._DepartureDate.HasValue != value.HasValue || (this._DepartureDate.HasValue == value.HasValue && (!value.HasValue || (this._DepartureDate.Value != value.Value))))
                {
                    this._DepartureDate = value;
                    this._DepartureDateChanged = true;
                    this.OnPropertyChanged("DepartureDate");
                }
            }
        }

        private string ValidateDepartureDate()
        {
            if (this._DepartureDateChanged)
            {
                if (!this.DepartureDate.HasValue)
                {
                    return "La fecha no debe estar vacia.";
                }
                //else if (DateTime.Now.AddDays(7).CompareTo(this.DepartureDate.Value) == -1)
                //{
                //    return "Departure can not be further than 7 days from today.";
                //}
                //else if (DateTime.Now.CompareTo(this.DepartureDate.Value) == 0)
                //{
                //    return "Departure can not be today.";
                //}
                //else if (DateTime.Now.CompareTo(this.DepartureDate.Value) == 1)
                //{
                //    return "Departure can not be in the past.";
                //}
            }
            return null;
        }

        
        public string Error
        {
            get
            {
                return ValidateDepartureDate();
            }
        }

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "DepartureDate": return this.ValidateDepartureDate();
                }
                return null;
            }
        }
    }
}
