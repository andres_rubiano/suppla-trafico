﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simplexity.Traffic.DTOs;
using Simplexity.Traffic.EF;
using Simplexity.Traffic.Constants;
using Simplexity.Traffic.Resources;

namespace Simplexity.Traffic.DomainLayer
{
    public class DTOMapper
    {
        /// <summary>
        /// Dado un DTO de TrafficLoad, rellena un Onjeto TfLoad del Entity
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="tfLoad"></param>
        public void MapTrafficLoad(TrafficLoadDTO dto, tfLoads tfLoad)
        {
            tfLoad.LtfLoad_LoaNumber = dto.LoadNumber;
            tfLoad.LtfSubsidiary_SubCode = dto.SubsidiarySubCode;
            tfLoad.LtfTruck_VehCode = dto.VehicleCode;
            tfLoad.LtfDriver_UcrCode = dto.DriverCode;
            tfLoad.LtfDriverName = dto.DriverName;
            tfLoad.LtfDriverLastName = dto.DriverLastName;
            tfLoad.LtfMonitoring_MonCode = dto.MonitoringGroupCode;
            tfLoad.LtfRoute_RouCode = dto.RouteCode;
            tfLoad.LtfStartDate = Utility.CastStringToValidDatetimeFormats(dto.StartDate);
            tfLoad.LtfCreationDate = DateTime.Now;
            tfLoad.LtfCreation_UsrCode = dto.CreationUsrCode;
            tfLoad.LtfLoaCarrier_UcrCode = dto.CarrierCode;
            tfLoad.LtfLoaCarrier_UcrName=dto.CarrierName;
            tfLoad.LtfLoaFrom_CitCode = dto.FromCode;
            tfLoad.LtfLoaFrom_CitName = dto.FromName;
            tfLoad.LtfLoaTo_CitCode = dto.ToCode;
            tfLoad.LtfLoaTo_CitName = dto.ToName;
            tfLoad.LtfLoaCustomerIdentification = dto.LoaCustomerIdentification;
            tfLoad.LtfLoaCustomerNames = dto.CustomerNames;
            tfLoad.LtfRoute_ExternalCode = dto.RouteExternalCode;
            tfLoad.LtfRouteVia = dto.RouteVia;
        }

        /// <summary>
        /// Dado un dto de CheckPoint, rellena un tfLoadcheckPoint del entity
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="LoadCheckPoint"></param>
        public void MapCheckpoint(CheckPointDTO dto, tfLoadCheckPoints LoadCheckPoint)
        {
            LoadCheckPoint.LitLoad_LoaNumber = dto.LoadNumber;
            LoadCheckPoint.LitCheckpoint_TrcCode  = dto.CheckpointTrcCode;
            LoadCheckPoint.LitCheckpoint_TrcDescription = dto.CheckpointTrcDescription;
            LoadCheckPoint.LitEstimatedDate = dto.EstimateDate;
            LoadCheckPoint.LitRealDate = dto.RealDate;
            LoadCheckPoint.LitLatitude = dto.Latitude;
            LoadCheckPoint.LitLongitude = dto.Longitude;
            LoadCheckPoint.LitType = dto.Type;
            LoadCheckPoint.LitLocation_UloCode = dto.LocationUloCode;
            LoadCheckPoint.LitCreationDate = DateTime.Now;
        }

        /// <summary>
        /// Dado un dto de Evento, rellena un tfLoadTrafficEvents del entity
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="LoadEvent"></param>
        public void MapEvent(EventDTO dto, tfLoadTrafficEvents LoadEvent)
        {
            LoadEvent.TfeCode = dto.TfeCode;
            LoadEvent.TfeCheckpoint_TrcCode = dto.TfeCheckpoint_TrcCode;
            LoadEvent.TfeDate = dto.TfeDate;
            LoadEvent.TfeLatitude = dto.TfeLatitude;
            LoadEvent.TfeLongitude = dto.TfeLongitude;
            LoadEvent.TfeShipment_ShiNumber = dto.TfeShipment_ShiNumber;
            LoadEvent.TfeLoad_LoaNumber = dto.TfeLoad_LoaNumber;
            LoadEvent.TfeLoadingOrder_LooNumber = dto.TfeLoadingOrder_LooNumber;
            LoadEvent.TfeType = dto.TfeType;
            LoadEvent.TfeText = dto.TfeText;
            LoadEvent.TfeLocation_UloCode = dto.TfeLocation_UloCode;
            LoadEvent.TfeLocationName = dto.TfeLocationName;
            LoadEvent.TfeSource = dto.TfeSource;
        }
      /// <summary>
        /// Dado un dto de requestService, rellena un TfRequestService del entity
      /// </summary>
      /// <param name="dto"></param>
      /// <param name="loaRequestService"></param>
        public void MapRequestService(RequestServiceDTO dto, TfRequestService loaRequestService)
          {
            loaRequestService.TfrInterface = dto.Interface;
            loaRequestService.TfrReference = dto.Reference;
            loaRequestService.TfrState = dto.State;
            loaRequestService.TfrRequestDate = dto.RequestDate;
            loaRequestService.TfrUserCode = dto.UserCode;
            loaRequestService.TfrResponse = dto.Response;
            loaRequestService.TfrProcessedDate = dto.ProcessedDate;
          }
    }
}
