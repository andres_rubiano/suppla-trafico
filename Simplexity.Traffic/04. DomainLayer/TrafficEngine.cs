﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.Traffic.DTOs;
using Simplexity.Traffic.EF;
using Simplexity.Traffic.Constants;
using Simplexity.Traffic.Resources;

//TODO : Esta pendiente ver que se hace con los tipos de puestos de control si es una tabla de tipos

namespace Simplexity.Traffic.DomainLayer
  {
    public class TrafficEngine
      {
        /// <summary>
        /// Metodo para calcular automaticamente la fecha estimada de los puestos de control teniendo en cuenta si tienen eventos de puesto cumplido
        /// y recalcular de alli en adelante
        /// Recibe la lista de puestos de control
        /// </summary>
        /// <param name="checkPoints"></param>
        /// <param name="load"></param>
        /// /// <returns></returns>
        public ResponseDTO EstimateCheckPointTimes(List<tfLoadCheckPoints> checkPoints, tfLoads load)
          {
            var responsedto = new ResponseDTO
                                {
                                  Message = new MessageDTO()
                                              {
                                                Type = 1,
                                                TypeEnum = MessagesConstants.Types.Information
                                              }
                                };
            // obtenemos el ultimo puesto de control con fecha real reportada:
            var lastCheckPoint = checkPoints.Where(p => p.LitRealDate != null).OrderByDescending(p => p.LitStdDelayMin).FirstOrDefault();

            if (lastCheckPoint == null)
              {
                // no hay puestos reportados o ya se reportaron todos:
                responsedto.Message.Type = 1;
                responsedto.Message.TypeEnum = MessagesConstants.Types.Information;
                responsedto.Message.Message = string.Format(Messages.LoadWithoutCheckpoints, load.LtfLoad_LoaNumber);
                return responsedto;
              }

            // puestos de control sin reporte, posteriores al ultimo reportado:
            //var cp = checkPoints.OrderBy(p => p.LitEstimatedDate).Where(p => p.LitEstimatedDate > lastCheckPoint.LitEstimatedDate);
            var cp = checkPoints.Where(p => p.LitRealDate == null && p.LitEstimatedDate > lastCheckPoint.LitEstimatedDate).OrderBy(p => p.LitStdDelayMin);
            try
              {
                #region OldCode

                //var numberOfCheckPoints = cp.Count();

                //  for (int i = numberOfCheckPoints - 1; i >= 0; i--)
                //    {
                //      if (cp.ElementAt(i).LitRealDate != null)
                //        {
                //          var timeDiff = (TimeSpan) (cp.ElementAt(i).LitRealDate - load.LtfStartDate);
                //          var cummDelay = (timeDiff.TotalMinutes - cp.ElementAt(i).LitStdDelayMin ?? 0);
                //          cp.ElementAt(i).LitNext = "F";
                //          if (((i + 1) <= numberOfCheckPoints - 1))
                //            {
                //              cp.ElementAt(i + 1).LitNext = "T";
                //            }

                //          for (var j = i; j < numberOfCheckPoints; j++)
                //            {
                //              cp.ElementAt(i).LitStdDelayMin += (int) cummDelay;
                //              cp.ElementAt(i).LitEstimatedDate = cp.ElementAt(i).LitEstimatedDate.AddMinutes(cummDelay);
                //            }
                //          break;
                //        }
                //    }
                //  return responsedto;

                #endregion

                #region NewCode

                // Se actualizan fechas estimadas:
                foreach (var cPoint in cp)
                  {
                    var delayMinutes = (double) (cPoint.LitStdDelayMin ?? 0);
                    cPoint.LitEstimatedDate = lastCheckPoint.LitRealDate.Value.AddMinutes((double) (delayMinutes - lastCheckPoint.LitStdDelayMin));
                  }

                // se marcan todos los puestos con 'F':
                foreach (var loadCheckPoints in checkPoints)
                  {
                    loadCheckPoints.LitNext = "F";
                  }

                // se marca el siguiente puesto de control:
                var nextCheckPoint =
                  cp.Where(cPoint => cPoint.LitRealDate == null && cPoint.LitStdDelayMin > lastCheckPoint.LitStdDelayMin)
                    .OrderBy(cPoint => cPoint.LitStdDelayMin)
                    .FirstOrDefault();
                if (nextCheckPoint != null) nextCheckPoint.LitNext = "T";

                return responsedto;

                #endregion
              }
            catch (Exception)
              {
                responsedto.Message.Type = 1;
                responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                return responsedto;
              }

          }

        /// <summary>
        /// revisa en la BBDD que no exista un evento igual
        /// </summary>
        /// <param name="eventHashCode"></param>
        /// <returns></returns>
        public ResponseDTO CheckEventNotExists(string eventHashCode)
          {
            var response = new ResponseDTO();
            using (var entity = new TrafficEntities())
              {
                var eve = (from e in entity.tfLoadTrafficEvents
                           where e.TfeHashCode == eventHashCode
                           select e).FirstOrDefault();
                if (eventHashCode != null)
                  {
                    response.Message.TypeEnum = MessagesConstants.Types.Error;
                  }

                return response;
              }
          }

        ///// <summary>
        ///// Metodo para calculo de los tiempos de la ruta ya deben existir los puestos de control
        ///// </summary>
        ///// <param name="load"></param>
        ///// <returns></returns>
        //public ResponseDTO CalculateRouteTime(tfLoads load)
        //{
        //    //volver esto un metodo de dominio, sin persistencia
        //    //y meterlo en el de arriba, donde esta indicado

        //    var db = new TrafficEntities();
        //    var responsedto = new ResponseDTO();

        //    try
        //    {

        //        var pload = (from l in db.tfLoads
        //                    where l.LtfLoad_LoaNumber == load.LtfLoad_LoaNumber
        //                    select l).FirstOrDefault();
        //        if (pload != null)
        //        {
        //            var litinerary = (from i in db.tfLoadCheckPoints
        //                              where i.LitLoad_LoaNumber == pload.LtfLoad_LoaNumber
        //                              orderby i.LitId
        //                              select i
        //                             );
        //            foreach (var loadItinerary in litinerary)
        //            {


        //                var sRouteCheckPoint = (from r in db.tfRouteCheckpoints
        //                                        where r.RxcRoute_RouCode == pload.LtfRoute_RouCode &&
        //                                              r.RxcCheckpoints_TrcCode == loadItinerary.LitCheckpoint_TrcCode
        //                                        select r).FirstOrDefault();

        //                loadItinerary.LitEstimatedDate = pload.LtfStartDate.Value.AddHours(sRouteCheckPoint.RxcStdDelayMin);

        //            }
        //        }
        //        else
        //        {
        //            responsedto.Message.Message = string.Format(Messages.LoadsTraffic_NotFound, pload.LtfLoad_LoaNumber);
        //            responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
        //            return responsedto;
        //        }

        //        using (var tscope = new TransactionScope())
        //        {
        //            //a new transaction will be created for this call
        //            db.SaveChanges();
        //            tscope.Complete();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        responsedto.Message.Message = ex.Message.ToString(CultureInfo.InvariantCulture);
        //        responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
        //        throw responsedto;
        //    }

        //    responsedto.Message.TypeEnum = MessagesConstants.Types.Information;
        //    responsedto.Message.TransactionNumber = load.LtfLoad_LoaNumber;
        //    return responsedto;
        //}
      }
  }