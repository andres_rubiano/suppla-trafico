﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Transactions;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Simplexity.Traffic.DTOs;
using Simplexity.Traffic.EF;
using Simplexity.Traffic.Constants;
using Simplexity.Traffic.Models;
using Simplexity.Traffic.Resources;
using Simplexity.Traffic.DomainLayer;
using Simplexity.Traffic._02._DTOs;


//TODO : Esta pendiente ver que se hace con los tipos de puestos de control si es una tabla de tipos
//TODO : Crear constantes para los estados
//Estos servicios se consumen desde WS, WCF, CodeGen JSON WS, u otros
//Contienen manejo de persistencia
//Interactuan hacia el exterior con DTOs principalmente

namespace Simplexity.Traffic
  {
    public class TrafficServices
      {
        /// <summary>
        /// Metodo para crear automaticamente los puestos de control de una viaje de trafico
        /// </summary>
        /// <param name="loadDTO"></param>
        /// <returns></returns>
        public ResponseDTO TrafficRoute_Initialize(TrafficLoadDTO loadDTO)
          {
            //TODO. Manejo del estado > 10
            //Mirar bien, parece que el entity actualizaría todo? incluido los checkpoints? o no

            var db = new TrafficEntities();
            var responseDTO = new ResponseDTO();
            var mapper = new DTOMapper();
            var load = new tfLoads();
            mapper.MapTrafficLoad(loadDTO, load);

            try
              {
                #region 1. Creamos o actualizamos el viaje de trafico

                // Si ya existe un trafico finalizado no se puede actualizar o crear:

                var finalizedTrafficLoad = (from l in db.tfLoads
                                            where
                                              l.LtfLoad_LoaNumber == load.LtfLoad_LoaNumber &&
                                              l.LtfTrafficState == (short) Constants.TrafficState.FinalizedTraffic
                                            select l).FirstOrDefault();

                if (finalizedTrafficLoad != null)
                  {
                    responseDTO.Message.Message = string.Format(Messages.LoadTrafficEnded, load.LtfLoad_LoaNumber);
                    responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responseDTO;
                  }

                // se puede adicionar o actualizar el viaje a trafico:

                var existingLoad = (from l in db.tfLoads
                                    where l.LtfLoad_LoaNumber == load.LtfLoad_LoaNumber && l.LtfTrafficState == (short) Constants.TrafficState.InTraffic
                                    select l).FirstOrDefault();

                if (existingLoad == null)
                  {
                    db.AddTotfLoads(load);
                  }
                else
                  {
                    //2015Sep09 Andres Rubiano:
                    // Si ya hay eventos reportados no se puede reescribir la informacion:

                    var existingTrafficEvents =
                      (from loaEvents in db.tfLoadTrafficEvents where loaEvents.TfeLoad_LoaNumber == load.LtfLoad_LoaNumber select loaEvents)
                        .FirstOrDefault();

                    if (existingTrafficEvents != null)
                      {
                        responseDTO.Message.Message = string.Format(Messages.LoadWithTrafficEvents, load.LtfLoad_LoaNumber);
                        responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                        return responseDTO;
                      }

                    // Si ya esta integrado con Destino seguro no se puede actualizar:

                    if (load.LtfNumberIntegration > 0)
                      {
                        responseDTO.Message.Message = string.Format(Messages.LoadWithExternalIntegration, load.LtfLoad_LoaNumber, "Destino Seguro");
                        responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                        return responseDTO;
                      }

                    existingLoad.LtfRoute_RouCode = load.LtfRoute_RouCode;
                    existingLoad.LtfLoad_LoaNumber = load.LtfLoad_LoaNumber;
                    existingLoad.LtfOperationalStatus = load.LtfOperationalStatus;
                    existingLoad.LtfTruck_VehCode = load.LtfTruck_VehCode;
                    existingLoad.LtfSubsidiary_SubCode = load.LtfSubsidiary_SubCode;
                    existingLoad.LtfDriver_UcrCode = load.LtfDriver_UcrCode;
                    existingLoad.LtfDriverName = load.LtfDriverName;
                    existingLoad.LtfDriverLastName = load.LtfDriverLastName;
                    existingLoad.LtfStartDate = load.LtfStartDate;
                    existingLoad.LtfEndDate = load.LtfEndDate;
                    existingLoad.LtfMonitoring_MonCode = load.LtfMonitoring_MonCode;
                    existingLoad.LtfLastTrafficEvent_TfeHashCode = load.LtfLastTrafficEvent_TfeHashCode;
                    existingLoad.LtfLoaCarrier_UcrCode = load.LtfLoaCarrier_UcrCode;
                    existingLoad.LtfLoaCarrier_UcrName = load.LtfLoaCarrier_UcrName;
                    existingLoad.LtfLoaFrom_CitCode = load.LtfLoaFrom_CitCode;
                    existingLoad.LtfLoaFrom_CitName = load.LtfLoaFrom_CitName;
                    existingLoad.LtfLoaTo_CitCode = load.LtfLoaTo_CitCode;
                    existingLoad.LtfLoaTo_CitName = load.LtfLoaTo_CitName;
                    existingLoad.LtfLoaCustomerIdentification = load.LtfLoaCustomerIdentification;
                    existingLoad.LtfLoaCustomerNames = load.LtfLoaCustomerNames;
                    existingLoad.LtfRoute_ExternalCode = load.LtfRoute_ExternalCode;
                    existingLoad.LtfRouteVia = load.LtfRouteVia;
                  }

                #endregion

                #region 2. Creamos el itinerario de la ruta primero borramos si existe y luego calculo e inserto

                // Aca borro si existen puestos de control

                var currentCheckPoints = (from i in db.tfLoadCheckPoints
                                          join tfLoad in db.tfLoads on i.LitLoad_LoaNumber equals tfLoad.LtfLoad_LoaNumber
                                          where
                                            i.LitLoad_LoaNumber == load.LtfLoad_LoaNumber &&
                                            tfLoad.LtfTrafficState == (short) Constants.TrafficState.InTraffic
                                          select i);

                foreach (var checkPoint in currentCheckPoints)
                  {
                    db.DeleteObject(checkPoint);
                  }

                var next = "T";

                // Creo la ruta:

                var routeCheckPoints = (from r in db.tfRouteCheckpoints
                                        where r.RxcRoute_RouCode == load.LtfRoute_RouCode
                                        orderby r.RxcKm
                                        select r);
                foreach (var routeCheckPoint in routeCheckPoints)
                  {
                    var baseCheckPoint = (from c in db.tfCheckpoints
                                          where c.TrcCode == routeCheckPoint.RxcCheckpoints_TrcCode
                                          select c).FirstOrDefault();
                    var newCheckPoint = new tfLoadCheckPoints
                                          {
                                            LitLoad_LoaNumber = load.LtfLoad_LoaNumber,
                                            LitType = "PC",
                                            LitCheckpoint_TrcCode = routeCheckPoint.RxcCheckpoints_TrcCode,
                                            LitCheckpoint_TrcDescription = baseCheckPoint.TrcDescription,
                                            LitLatitude = baseCheckPoint.TrcLat,
                                            LitLongitude = baseCheckPoint.TrcLong,
                                            LitCreationDate = DateTime.Now,
                                            LitCreation_UsrCode = load.LtfCreation_UsrCode,
                                            LitEstimatedDate = load.LtfStartDate.Value.AddMinutes(routeCheckPoint.RxcStdDelayMin),
                                            LitRealDate = null,
                                            LitAuto = "F",
                                            LitNext = next,
                                            LitStdDelayMin = routeCheckPoint.RxcStdDelayMin
                                          };

                    db.AddTotfLoadCheckPoints(newCheckPoint);
                    if (next == "T")
                      {
                        next = "F";
                      }
                  }

                #endregion

                #region Insertar "request" para integracion en DS:

                // Sea trafico nuevo o actualizado se hace el reques de envio a DS, si y solo si viene la ruta de DS:
                // la via si no existe simepre toma cero (0)

                if (!string.IsNullOrEmpty(load.LtfRoute_ExternalCode))
                  {
                    var loadRequestIntegration = new TfRequestService
                                                   {
                                                     TfrInterface = RequestInterface.LoadIntegration,
                                                     TfrReference = load.LtfLoad_LoaNumber,
                                                     TfrState = (short) RequestIntegrationState.Pending,
                                                     TfrRequestDate = DateTime.Now,
                                                     TfrUserCode = load.LtfCreation_UsrCode
                                                   };

                    db.AddToTfRequestService(loadRequestIntegration);
                  }

                #endregion

                using (var transaction = new TransactionScope())
                  {
                    //a new transaction will be created for this call
                    db.SaveChanges();
                    transaction.Complete();
                  }
              }
            catch (Exception ex)
              {
                responseDTO.Message.Message = ex.Message.ToString(CultureInfo.InvariantCulture);
                responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                return responseDTO;
              }

            responseDTO.Message.TypeEnum = MessagesConstants.Types.Information;
            responseDTO.Message.TransactionNumber = load.LtfLoad_LoaNumber;
            return responseDTO;
          }

        /// <summary>
        /// Pone fecha real a un puesto y recalcula el itinerario
        /// </summary>
        /// <param name="load"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public ResponseDTO CheckPoint_SetRealDate(CheckPointDTO checkPointDTO)
          {
            var responsedto = new ResponseDTO();
            var db = new TrafficEntities();
            var te = new TrafficEngine();

            //(tfLoads load, tfLoadCheckPoints checkPoint
            try
              {
                var load = (from l in db.tfLoads
                            where l.LtfLoad_LoaNumber == checkPointDTO.LoadNumber
                            select l).FirstOrDefault();

                #region validaciones

                if (load == null)
                  {
                    responsedto.Message.Message = string.Format(Messages.LoadsTraffic_NotFound, checkPointDTO.LoadNumber);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                  }
                var checkPoints = (from i in db.tfLoadCheckPoints
                                   where i.LitLoad_LoaNumber == checkPointDTO.LoadNumber
                                   orderby i.LitEstimatedDate
                                   select i
                  ).ToList();
                if (checkPoints.Count < 1)
                  {
                    responsedto.Message.Message = "No hay checkpoints para ese viaje... falta dar inicio?";
                    //TODO:string.Format(Messages.Checkpoint_NOTExists, checkPointDTO.CheckpointTrcCode);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                  }

                #endregion

                var checkpoint = checkPoints.FirstOrDefault(points => points.LitCheckpoint_TrcCode == checkPointDTO.CheckpointTrcCode);

                if (checkpoint != null)
                  checkpoint.LitRealDate = checkPointDTO.RealDate;
                else
                  {
                    responsedto.Message.Message = string.Format(Messages.Checkpoint_NOTExists, checkPointDTO.CheckpointTrcCode);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                  }


                // esto hay que corregirlo
                // la idea es tomar toda la lista entera de checkpoints
                // y pasarsela al metodo estimate, para que haga el trabajo de recalcularlos de una vez
                te.EstimateCheckPointTimes(checkPoints, load);

                using (var tscope = new TransactionScope())
                  {
                    //a new transaction will be created for this call
                    db.SaveChanges();
                    tscope.Complete();
                  }
              }
            catch (Exception ex)
              {
                responsedto.Message.Message = ex.Message.ToString(CultureInfo.InvariantCulture);
                responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                return responsedto;
              }

            responsedto.Message.Message = Messages.ProcessOk;
            responsedto.Message.TypeEnum = MessagesConstants.Types.Information;
            return responsedto;
          }

        /// <summary>
        /// Borra la fecha real de un puesto y recalcula la ruta
        /// </summary>
        /// <param name="checkPointDTO"></param>
        /// <returns></returns>
        public ResponseDTO CheckPoint_EraseRealDate(CheckPointDTO checkPointDTO)
          {
            var responsedto = new ResponseDTO();
            var db = new TrafficEntities();
            var te = new TrafficEngine();

            //(tfLoads load, tfLoadCheckPoints checkPoint
            try
              {
                var load = (from l in db.tfLoads
                            where l.LtfLoad_LoaNumber == checkPointDTO.LoadNumber
                            select l).FirstOrDefault();

                #region validaciones

                if (load == null)
                  {
                    responsedto.Message.Message = string.Format(Messages.LoadsTraffic_NotFound, checkPointDTO.LoadNumber);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                  }

                if (load.LtfOperationalStatus == 80)
                  {
                    responsedto.Message.Message = string.Format(Messages.LoadClosed, load.LtfLoad_LoaNumber);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                  }

                var checkPoints = (from i in db.tfLoadCheckPoints
                                   where //i.LitCheckpoint_TrcCode == checkPointDTO.CheckpointTrcCode &&
                                     i.LitLoad_LoaNumber == checkPointDTO.LoadNumber
                                   select i).ToList();
                if (checkPoints.Count < 1)
                  {
                    responsedto.Message.Message = "Ese viaje no tiene ningun checkpoint para borrar";
                    //string.Format(Messages.Checkpoint_NOTExists, checkPointDTO.CheckpointTrcCode);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                  }

                #endregion

                var checkpoint = checkPoints.FirstOrDefault(points => points.LitCheckpoint_TrcCode == checkPointDTO.CheckpointTrcCode);
                if (checkpoint != null)
                  checkpoint.LitRealDate = null;
                else
                  {
                    responsedto.Message.Message = string.Format(Messages.Checkpoint_NOTExists, checkPointDTO.CheckpointTrcCode);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                  }
                // esto hay que corregirlo para que compile. Ademas:

                // la idea es tomar toda la lista entera de checkpoints
                // y pasarsela al metodo estimate, para que haga el trabajo de recalcular todo nuevamente
                te.EstimateCheckPointTimes(checkPoints, load);


                using (var tscope = new TransactionScope())
                  {
                    //a new transaction will be created for this call
                    db.SaveChanges();
                    tscope.Complete();
                  }
              }
            catch (Exception ex)
              {
                responsedto.Message.Message = ex.Message.ToString(CultureInfo.InvariantCulture);
                responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                return responsedto;
              }

            responsedto.Message.Message = Messages.ProcessOk;
            responsedto.Message.TypeEnum = MessagesConstants.Types.Information;
            return responsedto;
          }

        /// <summary>
        /// Metodo para crear manualmente un puesto de control de un viaje
        /// </summary>
        /// <param name="loadNumber"></param>
        /// <param name="newCheckPoint"></param>
        /// <returns></returns>
        public ResponseDTO CheckPoint_UserAdd(string loadNumber, CheckPointDTO newCheckPointDTO)
          {
            //para sr un metodo externo, deberia recibir LoaNumber y un DTO con el puesto a insertar
            //mapearlo y que el dominio le llene el Objeto y persistir
            //como es una insercion directa, no recalcula la ruta, simplemente lo inserta

            var db = new TrafficEntities();
            var responseDTO = new ResponseDTO();
            var checkPoint = new tfLoadCheckPoints();
            var mapper = new DTOMapper();

            try
              {
                mapper.MapCheckpoint(newCheckPointDTO, checkPoint);

                var load = (from l in db.tfLoads
                            where l.LtfLoad_LoaNumber == loadNumber
                            select l).FirstOrDefault();
                if (load != null)
                  {
                    if (load.LtfOperationalStatus == 50)
                      {
                        responseDTO.Message.Message = string.Format(Messages.LoadClosed, checkPoint.LitCheckpoint_TrcCode);
                        responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                        return responseDTO;
                      }

                    var existingCheckPoint = (from i in db.tfLoadCheckPoints
                                              where i.LitCheckpoint_TrcCode == checkPoint.LitCheckpoint_TrcCode &&
                                                    i.LitLoad_LoaNumber == checkPoint.LitLoad_LoaNumber
                                              select i).FirstOrDefault();

                    if (existingCheckPoint != null)
                      {
                        responseDTO.Message.Message = string.Format(Messages.Checkpoint_Exists, checkPoint.LitCheckpoint_TrcCode);
                        responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                        return responseDTO;
                      }

                    checkPoint.LitAuto = "F";
                    db.AddTotfLoadCheckPoints(checkPoint);

                    using (var tscope = new TransactionScope())
                      {
                        //a new transaction will be created for this call
                        db.SaveChanges();
                        tscope.Complete();
                      }
                  }
                else
                  {
                    responseDTO.Message.Message = string.Format(Messages.LoadsTraffic_NotFound, loadNumber);
                    responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responseDTO;
                  }
              }
            catch (Exception ex)
              {
                responseDTO.Message.Message = ex.Message.ToString(CultureInfo.InvariantCulture);
                responseDTO.Message.TypeEnum = MessagesConstants.Types.Error;
                return responseDTO;
              }

            responseDTO.Message.Message = Messages.ProcessOk;
            responseDTO.Message.TypeEnum = MessagesConstants.Types.Information;
            return responseDTO;
          }

        /// <summary>
        /// Metodo para eliminar un puesto de control creado manualmente
        /// </summary>
        /// <param name="loads"></param>
        /// <param name="itinerary"></param>
        /// <returns></returns>
        public ResponseDTO CheckPoint_UserRemove(string loadNumber, string checkPointCode)
          {
            var db = new TrafficEntities();
            var responsedto = new ResponseDTO();

            try
              {
                var load = (from l in db.tfLoads
                            where l.LtfLoad_LoaNumber == loadNumber
                            select l).FirstOrDefault();
                if (load != null)
                  {
                    var checkPoint = (from i in db.tfLoadCheckPoints
                                      where i.LitCheckpoint_TrcCode == checkPointCode &&
                                            i.LitLoad_LoaNumber == loadNumber
                                      select i
                      ).FirstOrDefault();
                    if (checkPoint != null)
                      {
                        responsedto.Message.Message = string.Format(Messages.Checkpoint_Exists, checkPoint.LitCheckpoint_TrcCode);
                        responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                        if (checkPoint.LitRealDate != null)
                          {
                            responsedto.Message.Message = string.Format(Messages.Checkpoint_Reported, checkPoint.LitCheckpoint_TrcCode);
                            responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                            return responsedto;
                          }
                      }

                    db.DeleteObject(checkPoint);
                    using (var transaction = new TransactionScope())
                      {
                        db.SaveChanges();
                        transaction.Complete();
                      }
                  }
                else
                  {
                    responsedto.Message.Message = string.Format(Messages.LoadsTraffic_NotFound, loadNumber);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                  }
              }
            catch (Exception ex)
              {
                responsedto.Message.Message = ex.Message.ToString(CultureInfo.InvariantCulture);
                responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                return responsedto;
              }

            responsedto.Message.Message = Messages.ProcessOk;
            responsedto.Message.TypeEnum = MessagesConstants.Types.Information;

            return responsedto;
          }

        /// <summary>
        /// Registra un nuevo evento
        /// </summary>
        /// <param name="loadEvent"></param>
        /// <returns></returns>
        public ResponseDTO Event_Register(EventDTO loadEventDTO)
          {
            var response = new ResponseDTO();
            var mapper = new DTOMapper();
            var db = new TrafficEntities();

            try
              {
                var load = (from l in db.tfLoads
                            where l.LtfLoad_LoaNumber == loadEventDTO.TfeLoad_LoaNumber
                            select l).FirstOrDefault();


                var loadEvent = new tfLoadTrafficEvents();

                mapper.MapEvent(loadEventDTO, loadEvent);

                #region validaciones

                if (load == null)
                  {
                    response.Message.Message = string.Format(Messages.LoadsTraffic_NotFound, loadEventDTO.TfeLoad_LoaNumber);
                    response.Message.TypeEnum = MessagesConstants.Types.Error;
                    return response;
                  }

                if (load.LtfOperationalStatus == 80)
                  {
                    response.Message.Message = string.Format(Messages.LoadClosed, load.LtfLoad_LoaNumber);
                    response.Message.TypeEnum = MessagesConstants.Types.Error;
                    return response;
                  }

                #endregion

                using (var transaction = new TransactionScope())
                  {
                    #region

                    // aqui disparar la accion segun el evento se revisa operaciones segun tipo de evento de trafico Event TfeType (UNITY o SPRING)
                    // quien marca el evento como procesado? esto sería parte del motor de interfaz externo

                    loadEventDTO.TfeHashCode = Guid.NewGuid().ToString();
                    loadEventDTO.TfeCode = Guid.NewGuid().ToString();
                    //Campos agregados
                    loadEventDTO.TfDriverName = load.LtfDriverName;
                    loadEventDTO.TfDriver_UcrCode = load.LtfDriver_UcrCode;
                    loadEventDTO.TfRoute_RouCode = load.LtfRoute_RouCode;
                    loadEventDTO.TfState = load.LtfOperationalStatus;
                    loadEventDTO.TfTruck_VehCode = load.LtfTruck_VehCode;

                    IUnityContainer container = new UnityContainer();
                    container.LoadConfiguration();

                    var actionModel = container.Resolve<IModel>(loadEventDTO.TfeType);
                    response = actionModel.Action(loadEventDTO, db);

                    if (response.Message.TypeEnum == MessagesConstants.Types.Error)
                      {
                        return response;
                      }

                    #endregion

                    // Antes de guardar, de acuerdo al tipo de evento actualizamos datos del viaje:

                    switch (loadEventDTO.TfeType)
                      {
                        case "FinTra":
                          load.LtfTrafficState = (short) TrafficState.FinalizedTraffic;
                          break;
                      }

                    db.SaveChanges();
                    transaction.Complete();
                  }
                return response;
              }
            catch (Exception ex)
              {
                response.Message.Message = ex.Message.ToString(CultureInfo.InvariantCulture);
                  if (ex.InnerException != null)
                      response.Message.Message = string.Format("{0}; {1}", response.Message,
                          ex.InnerException.Message.ToString(CultureInfo.InvariantCulture));

                response.Message.TypeEnum = MessagesConstants.Types.Error;
                return response;
              }
          }


        public ResponseDTO CancelLoad(string loadNumber)
          {
            var db = new TrafficEntities();
            var responsedto = new ResponseDTO();

            try
              {
                var load = (from l in db.tfLoads
                            where l.LtfLoad_LoaNumber == loadNumber && l.LtfTrafficState == (short) Constants.TrafficState.InTraffic
                            select l).FirstOrDefault();


                if (load == null)
                  {
                    // no hay trafico para el viaje.. el mensaje es solo de informacion.
                    responsedto.Message.Message = string.Format(Messages.LoadsTraffic_NotFound, loadNumber);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Information;
                    return responsedto;
                  }

                // Verifica si hay eventos registrados:

                var events = (from l in db.tfLoadTrafficEvents
                              where l.TfeLoad_LoaNumber == loadNumber
                              select l);

                if (events.FirstOrDefault() != null)
                  {
                    // si hay eventos no se puede cancelar:
                    responsedto.Message.Message = string.Format(Messages.LoadWithTrafficEvents, load.LtfLoad_LoaNumber);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                  }

                // El viaje existe en trafico pero no tiene eventos reportados, se puede cancelar, toca eliminar el registro por temas de constrain:

                db.DeleteObject(load);
                //load.LtfTrafficState = (short) TrafficState.CanceledTraffic;

                //eliminamos puestos de control:

                var loadCheckPoints = (from cPoints in db.tfLoadCheckPoints where cPoints.LitLoad_LoaNumber == load.LtfLoad_LoaNumber select cPoints);
                foreach (var cPoints in loadCheckPoints)
                  {
                    db.DeleteObject(cPoints);
                  }

                responsedto.Message.TypeEnum = MessagesConstants.Types.Information;
              }
            catch (Exception ex)
              {
                responsedto.Message.Message = ex.Message.ToString(CultureInfo.InvariantCulture);
                responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                return responsedto;
              }


            using (var transaction = new TransactionScope())
              {
                //a new transaction will be created for this call
                db.SaveChanges();
                transaction.Complete();
              }

            return responsedto;
          }

        public List<TrafficLoadDTO> GetTrafficLoads()
          {
            using (var db = new TrafficEntities())
              {
                var loads = (from trafficLoads in db.tfLoads
                             where trafficLoads.LtfTrafficState == (short) TrafficState.InTraffic
                             select trafficLoads).ToList().Select(trafficLoads => new TrafficLoadDTO()
                                                                                    {
                                                                                      DriverCode = trafficLoads.LtfDriver_UcrCode,
                                                                                      DriverName = trafficLoads.LtfDriverName,
                                                                                      LoadNumber = trafficLoads.LtfLoad_LoaNumber,
                                                                                      RouteCode = trafficLoads.LtfRoute_RouCode,
                                                                                      StartDate =
                                                                                        Convert.ToDateTime(trafficLoads.LtfStartDate)
                                                                                               .ToString("yyyy/MM/dd HH:mm:ss"),
                                                                                      VehicleCode = trafficLoads.LtfTruck_VehCode,
                                                                                      CarrierName = trafficLoads.LtfLoaCarrier_UcrName,
                                                                                      ToCode = trafficLoads.LtfLoaTo_CitCode,
                                                                                      ToName = trafficLoads.LtfLoaTo_CitName,
                                                                                      FromName = trafficLoads.LtfLoaFrom_CitName,
                                                                                      FromCode = trafficLoads.LtfLoaFrom_CitCode,
                                                                                      CustomerNames = trafficLoads.LtfLoaCustomerNames
                                                                                    }).ToList();

                foreach (var load in loads)
                  {
                    var query = (from c in db.tfLoadCheckPoints
                                 where c.LitNext.Equals("T") && c.LitLoad_LoaNumber == load.LoadNumber
                                 select c.LitEstimatedDate).FirstOrDefault();
                    load.Delay = (DateTime.Now - query).TotalMinutes;

                    var lastEvent = (from events in db.tfLoadTrafficEvents
                                     where events.TfeLoad_LoaNumber == load.LoadNumber
                                     orderby events.TfeId descending
                                     select events.TfeType).FirstOrDefault();

                    var eventDescription = (from eventsType in db.tfTrafficEventType
                                            where eventsType.TetCode == lastEvent
                                            select eventsType.TetName).FirstOrDefault();

                    load.LastEvent = string.IsNullOrEmpty(lastEvent) ? "Sin eventos" : eventDescription;
                  }
                return loads;
              }
          }

        public List<CheckPointDTO> GetCheckpointsbyLoad(string loanumber)
          {
            using (var db = new TrafficEntities())
              {
                var checkpoints = (from l in db.tfLoads
                                   join c in db.tfLoadCheckPoints on l.LtfLoad_LoaNumber equals c.LitLoad_LoaNumber
                                   where l.LtfLoad_LoaNumber == loanumber
                                   orderby c.LitEstimatedDate
                                   select new CheckPointDTO
                                            {
                                              LoadNumber = c.LitLoad_LoaNumber,
                                              UserCode = c.LitCreation_UsrCode,
                                              Type = c.LitType,
                                              CheckpointTrcCode = c.LitCheckpoint_TrcCode,
                                              CheckpointTrcDescription = c.LitCheckpoint_TrcDescription,
                                              CreationDate = c.LitCreationDate ?? DateTime.MinValue,
                                              EstimateDate = c.LitEstimatedDate,
                                              LastEvent = c.LitLastTrafficEvent_TfeId ?? 0,
                                              Latitude = c.LitLatitude,
                                              LocationUloCode = c.LitLocation_UloCode,
                                              Longitude = c.LitLongitude,
                                              RealDate = c.LitRealDate ?? DateTime.MinValue
                                            }).ToList();
                return checkpoints;
              }
          }

        public List<EventDTO> GetEventsbyLoad(string loanumber)
          {
            using (var db = new TrafficEntities())
              {
                return (from l in db.tfLoads
                        join e in db.tfLoadTrafficEvents on l.LtfLoad_LoaNumber equals e.TfeLoad_LoaNumber
                        join m in db.tfTrafficEventType on e.TfeType equals m.TetCode
                        where e.TfeLoad_LoaNumber == loanumber
                        orderby e.TfeDate descending
                        select new EventDTO
                                 {
                                   TfeCheckpoint_TrcCode = e.TfeCheckpoint_TrcCode,
                                   TfeCode = e.TfeCode,
                                   TfeDate = e.TfeDate ?? DateTime.MinValue,
                                   TfeHashCode = e.TfeHashCode,
                                   TfeLatitude = e.TfeLatitude,
                                   TfeLoad_LoaNumber = e.TfeLoad_LoaNumber,
                                   TfeLoadingOrder_LooNumber = e.TfeLoadingOrder_LooNumber,
                                   TfeLocationName = e.TfeLocationName,
                                   TfeLocation_UloCode = e.TfeLocation_UloCode,
                                   TfeLongitude = e.TfeLongitude,
                                   TfeShipment_ShiNumber = e.TfeShipment_ShiNumber,
                                   TfeText = e.TfeText,
                                   TfeType = e.TfeType,
                                   TfeTypeDescription = m.TetName,
                                   TfeCreation_UsrCode = e.TfeCreation_UsrCode,
                                   TfeSource = e.TfeSource
                                 }).ToList();
              }
          }

        public List<EventTypeDTO> GetEventTypes()
          {
            using (var db = new TrafficEntities())
              return (db.tfTrafficEventType.Where(p => p.TetActive == "T").Select(a => new EventTypeDTO()
                                                                                         {
                                                                                           TetCode = a.TetCode
                                                                                           ,
                                                                                           TetCodeModel = a.TetCodeModel
                                                                                           ,
                                                                                           TetDescription = a.TetDescription
                                                                                           ,
                                                                                           TetName = a.TetName
                                                                                         })).OrderBy(p => p.TetName).ToList();
          }

        public List<VehiclesGpsDto> GetVehiclesInfo()
          {
            using (var db = new TrafficEntities())
              return (db.tfLoadCheckPointsView.Select(a => new VehiclesGpsDto()
                                                             {
                                                               Deltac = a.Deltac ?? 0,
                                                               Latitude = a.LitLatitude,
                                                               Longitude = a.LitLongitude,
                                                               VehCode = a.VehicleCode,
                                                               Status = a.Estado,
                                                               GeoRef = a.georef
                                                             })).ToList();
          }

        public string GetUsrcode(string session)
          {
            using (var db = new TrafficEntities())
              {
                var firstOrDefault = db.adSessions.FirstOrDefault(p => p.SesCode == session);
                return firstOrDefault != null ? (firstOrDefault.Ses_UsrCode) : "";
              }
          }
      }
  }