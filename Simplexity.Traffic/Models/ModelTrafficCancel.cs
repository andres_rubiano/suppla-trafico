﻿using System;
using System.Globalization;
using System.Linq;
using Simplexity.Traffic.Constants;
using Simplexity.Traffic.DTOs;
using Simplexity.Traffic.EF;
using Simplexity.Traffic.Resources;

namespace Simplexity.Traffic.Models
  {
    public class ModelTrafficCancel : IModel

      {
        public ResponseDTO Action(EventDTO loadEventDTO, TrafficEntities db)
          {
            var responsedto = new ResponseDTO
                                {
                                  Message = new MessageDTO
                                              {
                                                TypeEnum = MessagesConstants.Types.Information
                                              }
                                };
            try
              {
                var load = (from l in db.tfLoads
                            where l.LtfLoad_LoaNumber == loadEventDTO.TfeLoad_LoaNumber && l.LtfTrafficState == (short) Constants.TrafficState.InTraffic
                            select l).FirstOrDefault();


                if (load == null)
                  {
                    // no hay trafico para el viaje.. el mensaje es solo de informacion.
                    responsedto.Message.Message = string.Format(Messages.LoadsTraffic_NotFound, loadEventDTO.TfeLoad_LoaNumber);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Information;
                    return responsedto;
                  }

                // Verifica si hay eventos registrados:

                var events = (from l in db.tfLoadTrafficEvents
                              where l.TfeLoad_LoaNumber == loadEventDTO.TfeLoad_LoaNumber
                              select l);

                if (events.FirstOrDefault() != null)
                  {
                    // si hay eventos no se puede cancelar:
                    responsedto.Message.Message = string.Format(Messages.LoadWithTrafficEvents, load.LtfLoad_LoaNumber);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                  }

                // El viaje existe en trafico pero no tiene eventos reportados, se puede cancelar:

                load.LtfTrafficState = (short) TrafficState.CanceledTraffic;

                // Se eliminan puestos de control:

                var trafficCheckPoints = (from cPoint in db.tfLoadCheckPoints
                                          where cPoint.LitLoad_LoaNumber == loadEventDTO.TfeLoad_LoaNumber
                                          select cPoint);

                foreach (var loadCheckP in trafficCheckPoints)
                  {
                    db.DeleteObject(loadCheckP);
                  }

                responsedto.Message.TypeEnum = MessagesConstants.Types.Information;
              }
            catch (Exception ex)
              {
                responsedto.Message.Message = ex.Message.ToString(CultureInfo.InvariantCulture);
                responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                return responsedto;
              }

            return responsedto;
          }
      }
  }