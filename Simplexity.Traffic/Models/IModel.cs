﻿using Simplexity.Traffic.DTOs;
using Simplexity.Traffic.EF;

namespace Simplexity.Traffic.Models
{
    public interface IModel
    {
        ResponseDTO Action(EventDTO loadEventDTO, TrafficEntities db);
    }
}
