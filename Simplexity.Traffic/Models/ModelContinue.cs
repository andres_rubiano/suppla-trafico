﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Simplexity.Traffic.Constants;
using Simplexity.Traffic.DTOs;
using Simplexity.Traffic.DomainLayer;
using Simplexity.Traffic.EF;
using Simplexity.Traffic.Resources;

namespace Simplexity.Traffic.Models
{
    public class ModelContinue:IModel
    {
        public ResponseDTO Action(EventDTO loadEventDTO, TrafficEntities db)
        {
            var te = new TrafficEngine();
            var responsedto = new ResponseDTO
            {
                Message = new MessageDTO
                {
                    TypeEnum = MessagesConstants.Types.Information
                }
            };
            try
            {
                var load = (from l in db.tfLoads
                            where l.LtfLoad_LoaNumber == loadEventDTO.TfeLoad_LoaNumber
                            select l).FirstOrDefault();
                var checkPoints = (from i in db.tfLoadCheckPoints
                                   where i.LitLoad_LoaNumber == loadEventDTO.TfeLoad_LoaNumber
                                   select i).ToList();

                if ( checkPoints.Count > 0 && load != null)
                {
                    responsedto = te.EstimateCheckPointTimes(checkPoints, load);
                    db.AddTotfLoadTrafficEvents(new tfLoadTrafficEvents
                    {
                        TfeCode = loadEventDTO.TfeCode
                        ,TfeHashCode = loadEventDTO.TfeHashCode
                        ,TfeCreationDate = DateTime.Now
                        ,TfeCreation_UsrCode = loadEventDTO.TfeCreation_UsrCode
                        ,TfeCheckpoint_TrcCode = loadEventDTO.TfeCheckpoint_TrcCode
                        ,TfeLatitude = loadEventDTO.TfeLatitude
                        ,TfeLoad_LoaNumber = loadEventDTO.TfeLoad_LoaNumber
                        ,TfeLoadingOrder_LooNumber = loadEventDTO.TfeLoadingOrder_LooNumber
                        ,TfeLocationName = loadEventDTO.TfeLocationName
                        ,TfeLocation_UloCode = loadEventDTO.TfeLocation_UloCode
                        ,TfeDate = loadEventDTO.TfeDate
                        ,TfeLongitude = loadEventDTO.TfeLongitude
                        ,TfeShipment_ShiNumber = loadEventDTO.TfeShipment_ShiNumber
                        ,TfeText = loadEventDTO.TfeText
                        ,TfeType = loadEventDTO.TfeType
                        ,TfDriverName = loadEventDTO.TfDriverName
                        ,TfDriver_UcrCode = loadEventDTO.TfDriver_UcrCode
                        ,TfRoute_RouCode = loadEventDTO.TfRoute_RouCode
                        ,TfState = loadEventDTO.TfState
                        ,TfTruck_VehCode = loadEventDTO.TfTruck_VehCode,
                        TfeSource = loadEventDTO.TfeSource
                    });
                    load.LtfLastTrafficEvent_TfeHashCode = loadEventDTO.TfeHashCode;
                }
                else
                {
                    responsedto.Message.Message = string.Format(Messages.Checkpoint_NOTExists,
                                                                loadEventDTO.TfeCheckpoint_TrcCode);
                    responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                    return responsedto;
                }
            }
            catch (Exception ex)
            {
                responsedto.Message.Message = ex.Message.ToString(CultureInfo.InvariantCulture);
                responsedto.Message.TypeEnum = MessagesConstants.Types.Error;
                return responsedto;
            }
            return responsedto;
        }
    }
}
