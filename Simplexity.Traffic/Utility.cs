﻿using System;
using System.Collections;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Simplexity.Traffic.Resources;

namespace Simplexity.Traffic
{
  public static class Utility
  {

    public static DateTime CastStringToDatetime(string dateTimeCandidate, string datetimeFormat)
    {
      DateTime datetime;
      string formatString = datetimeFormat;
      CultureInfo provider = CultureInfo.InvariantCulture;

      try
      {
        datetime = DateTime.ParseExact(dateTimeCandidate, formatString, provider);
      }
      catch (FormatException e)
      {

        datetime = DateTime.MinValue;
      }
      return datetime;

    }

    /// <summary>
    /// Convierte una cadena texto a Datetime. La cadena texto debe estar en alguno de los siguientes formatos: yyyy/MM/dd HH:mm:ss ,
    /// yyyy.MM.dd HH:mm:ss , yyyy-MM-dd HH:mm:ss , yyyyMMdd HH:mm:ss
    /// siempre se envìan las horas en formato militar (HH)
    /// </summary>
    /// <param name="dateTimeCandidate"></param>
    /// <returns></returns>
    public static DateTime CastStringToValidDatetimeFormats(string dateTimeCandidate)
    {
      DateTime datetime = DateTime.MinValue;
      if (string.IsNullOrEmpty(dateTimeCandidate))
      {
        return DateTime.MinValue;
      }
      datetime = CastStringToDatetime(dateTimeCandidate, "yyyy/MM/dd HH:mm:ss");
      if (datetime != DateTime.MinValue)
      {
        return datetime;
      }
      datetime = CastStringToDatetime(dateTimeCandidate, "yyyy.MM.dd HH:mm:ss");
      if (datetime != DateTime.MinValue)
      {
        return datetime;
      }
      datetime = CastStringToDatetime(dateTimeCandidate, "yyyy-MM-dd HH:mm:ss");
      if (datetime != DateTime.MinValue)
      {
        return datetime;
      }
      datetime = CastStringToDatetime(dateTimeCandidate, "yyyyMMdd HH:mm:ss");
      if (datetime != DateTime.MinValue)
      {
        return datetime;
      }
      return datetime;
    }



    /// <summary>
    /// Crea un archivo Separado por Comas a partir  
    /// </summary>
    /// <param name="dtTable">DataTable</param>
    /// <param name="filePath">Ruta del archivo </param>
    public static void CreateCSVFile(DataTable dtTable, string filePath)
    {
      using (var outfile = new StreamWriter(filePath, true))
      {
        //Crea Los Nombres Encabazados
        var arrColumnas = new string[dtTable.Columns.Count];
        var sbHead = new StringBuilder();


        for (int i = 0; i < dtTable.Columns.Count; i++)
        {
          arrColumnas[i] = dtTable.Columns[i].ColumnName;
        }

        sbHead.Append(string.Join(",", arrColumnas));
        outfile.WriteLine(sbHead.ToString());

        // Crea las Lineas
        foreach (DataRow row in dtTable.Rows)
        {
          var sb = new StringBuilder();
          sb.Append(string.Join(",", row.ItemArray));
          outfile.WriteLine(sb.ToString());

        }
      }


    }

    /// <summary>
    /// Sends an email by using the XSLT template file. The template file is transform over a IDictionary object
    /// to an XHTML document. The contents in the subject line are replaced by the contents of title tag
    /// and the html body becomes the email contents.
    /// </summary>
    /// <param name="emailto">To email address</param>
    /// <param name="xslttemplatename">XSLT template file name</param>
    /// <param name="objDictionary">Dictonary objects containing data to be inserted in the transformed doc.</param>
    public static string FormatEmail(IDictionary objDictionary, byte[] byteArray)
    {
      try
      {
        // Instantiate a new XslTransform object and load the style sheet. 
        Stream xmlFile = new MemoryStream(byteArray);
        XmlReader xmlt = new XmlTextReader(xmlFile);
        var objxslt = new XslTransform();
        objxslt.Load(xmlt);

        var xmldoc = new XmlDocument();
        xmldoc.AppendChild(xmldoc.CreateElement("DocumentRoot"));
        var xpathnav = xmldoc.CreateNavigator();

        // Instantiate an XsltArgumentList object.
        // An XsltArgumentList object is used to supply extension object instances
        // and values for XSLT paarmeters required for an XSLT. transformation	 
        var xslarg = new XsltArgumentList();

        //Add an instance of the extension object to the XsltArgumentList.
        //The AddExtensionObject method is used to add the Extension object instance to the
        //XsltArgumentList object. The namespace URI specified as the first parameter 
        //should match the namespace URI used to reference the Extension object in the
        //XSLT style sheet.
        if (objDictionary != null)
        {
          foreach (DictionaryEntry entry in objDictionary)
          {
            xslarg.AddExtensionObject(entry.Key.ToString(), entry.Value);
          }
        }

        var emailbuilder = new StringBuilder();
        var xmlwriter = new XmlTextWriter(new System.IO.StringWriter(emailbuilder));

        // Execute the transformation and generate the output to the Response object's (xmlwriter)
        // output stream. Notice how the XsltArgumentList object to which the Extension
        // object instance was added is supplied as a parameter when executing the
        // Transform method of the XslTransform object. 
        objxslt.Transform(xpathnav, xslarg, xmlwriter, null);

        string subjecttext, bodytext;

        var xemaildoc = new XmlDocument();
        xemaildoc.LoadXml(emailbuilder.ToString());
        var titlenode = xemaildoc.SelectSingleNode("//title");

        subjecttext = titlenode.InnerText;

        var bodynode = xemaildoc.SelectSingleNode("//body");

        bodytext = bodynode.InnerXml;
        if (bodytext.Length > 0)
        {
          bodytext = bodytext.Replace("&amp;", "&");
        }

        return bodytext;
      }
      // Exception handling code.
      catch (XsltCompileException xsltCompileExp)
      {
        string message = (xsltCompileExp.Message);
      }
      catch (XsltException xsltExp)
      {
        string message = (xsltExp.Message);
      }
      catch (XPathException XPathExp)
      {
        string message = (XPathExp.Message);
      }
      catch (XmlException XmlExp)
      {
        string message = (XmlExp.Message);
      }

      return string.Empty;

    }

    public static void SendMail(string to, string subject, string body)
    {
      //var email = new Simplexity.Email.Email("EmailSettings");

      //email.SendMail(to, subject, body);
    }

    public static void SendMailAttachment(string to, string subject, string body, string fileName)
    {

      //var email = new Simplexity.Email.Email("EmailSettings");
      //email.SendMailAttachment(to, subject, body, fileName);
    }

    public static DateTime CheckDateFormat(string datetimeCandidate)
    {
      var datetime = CastStringToValidDatetimeFormats(datetimeCandidate);

      if (datetime == DateTime.MinValue)
      {
        throw new ArgumentException(String.Format(Messages.warning_InvalidDate, datetimeCandidate));
      }
      return datetime;
    }



  }
}
