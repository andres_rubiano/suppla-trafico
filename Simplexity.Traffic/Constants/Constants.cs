﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.Traffic.Constants
  {
    public class MessagesConstants
      {
        public enum Types
          {
            None = 0,
            Information = 1,
            Warning = 2,
            Error = 3
          }
      }

    public enum TrafficState
      {
        //-- Estados de trafico   0 = En trafico
        //--                      1 = Trafico finalizado
        //--                    -99 = Trafico cancelado.
        InTraffic = 0,
        FinalizedTraffic = 1,
        CanceledTraffic = -99
      }

    public enum RequestIntegrationState
      {
        // Estados del proceso de integracion con DS:
        Pending = 0,
        InProcess = 1,
        FailedProcessing = -99,
        ProcessedCorrectly = 2
      }

    public static class RequestInterface
      {
        public static string LoadIntegration
          {
            get { return "LoadInt"; }
          }
      }
  }