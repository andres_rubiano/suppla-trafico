﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.Traffic.DTOs
{
    public class EventDTO
    {
        public string TfeCode { get; set; }
        public string TfeLoad_LoaNumber { get; set; }
        public string TfeShipment_ShiNumber { get; set; }
        public string TfeLoadingOrder_LooNumber { get; set; }
        public string TfeType { get; set; }
        public string TfeTypeDescription { get; set; }
        public string TfeText { get; set; }
        public DateTime TfeDate { get; set; }
        public string TfeCheckpoint_TrcCode { get; set; }
        public string TfeLocation_UloCode { get; set; }
        public string TfeLocationName { get; set; }
        public string TfeLatitude { get; set; }
        public string TfeLongitude { get; set; }
        public string TfeHashCode { get; set; }
        public string TfeCreation_UsrCode { get; set; }
        public string TfRoute_RouCode { get; set; }
        public string TfTruck_VehCode { get; set; }
        public string TfDriver_UcrCode { get; set; }
        public string TfDriverName { get; set; }
        public short TfState { get; set; }
        public short TfeSource { get; set; }
    }
}
