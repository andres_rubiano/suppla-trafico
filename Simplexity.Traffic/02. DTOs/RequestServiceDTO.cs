﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.Traffic.DTOs
  {
    public class RequestServiceDTO
      {
        public string Interface { get; set; } //[TfrInterface] [varchar] (10)  NULL,
        public string Reference { get; set; } //[TfrReference] [varchar] (max)  NULL,
        public int State { get; set; } //[TfrState] [int] NULL,
        public DateTime RequestDate { get; set; } //[TfrRequestDate] [datetime] NULL,
        public string UserCode { get; set; } //[TfrUserCode] [varchar] (20)  NULL,
        public string Response { get; set; } //[TfrResponse] [varchar] (max) NULL,
        public DateTime ProcessedDate { get; set; } //[TfrProcessedDate] [datetime] NULL
      }
  }