﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Simplexity.Traffic.DTOs
{
    public class TrafficLoadDTO
    {
        [DataMember]
        public string LoadNumber { get; set; }
        [DataMember]
        public string SubsidiarySubCode { get; set; }
        [DataMember]
        public string StartDate { get; set; }
        [DataMember]
        public string RouteCode { get; set; }
        [DataMember]
        public string VehicleCode { get; set; }
        [DataMember]
        public string DriverCode { get; set; }
        [DataMember]
        public string DriverName { get; set; }
        [DataMember]
        public string DriverLastName { get; set; }
        [DataMember]
        public string MonitoringGroupCode { get; set; }
        [DataMember]
        public string CreationUsrCode { get; set; }
        [DataMember]
        public double Delay { get; set; }
        [DataMember]
        public string LastEvent{ get; set; }
        [DataMember]
        public string CarrierCode { get; set; }
        [DataMember]
        public string CarrierName { get; set; }
        [DataMember]
        public string FromCode { get; set; }
        [DataMember]
        public string FromName { get; set; }
        [DataMember]
        public string ToCode { get; set; }
        [DataMember]
        public string ToName { get; set; }
        [DataMember]
        public string LoaCustomerIdentification { get; set; }
        [DataMember]
        public string CustomerNames { get; set; }
        [DataMember]
        public string RouteExternalCode { get; set; }
        [DataMember]
        public int RouteVia { get; set; }
    }
}
