﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.Traffic.DTOs
{
    public class VehiclesGpsDto
    {
        public string VehCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public double Direction { get; set; }
        public double Speed { get; set; }
        public string GeoRef { get; set; }
        public string GpsDate { get; set; }
        public int Deltac { get; set; }
        public string Status { get; set; }
    }

}
