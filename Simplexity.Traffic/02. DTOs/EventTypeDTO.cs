﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.Traffic._02._DTOs
{
    public class EventTypeDTO
    {
        public string TetCode { get; set; }
        public string TetName { get; set; }
        public string TetDescription { get; set; }
        public string TetCodeModel { get; set; }
        public string TetActive { get; set; }
    }
}
