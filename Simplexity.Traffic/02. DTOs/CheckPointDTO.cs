﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.Traffic.DTOs
{
    public class CheckPointDTO
    {
        private DateTime? _realDate;
        public string LoadNumber { get; set; }
        public string CheckpointTrcCode { get; set; }
        public string CheckpointTrcDescription { get; set; }
        public DateTime EstimateDate { get; set; }
        public DateTime? RealDate
        {
            get{ return _realDate.Equals(DateTime.MinValue) ? null : _realDate; }
            set { _realDate = value; }
        }

        public string LocationUloCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Type { get; set; }
        public int LastEvent { get; set; }
        public string UserCode { get; set; }
        public DateTime CreationDate { get; set; }
    }
}
