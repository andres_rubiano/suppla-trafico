﻿using System.Xml.Serialization;
using Simplexity.Traffic.Constants;

namespace Simplexity.Traffic.DTOs
{
    public class MessageDTO
    {
        public MessageDTO()
        {
            TypeEnum = MessagesConstants.Types.Information;
        }
        [XmlIgnore]
        public MessagesConstants.Types TypeEnum { set; get; }

        public int Type
        {
            get { return (int)TypeEnum; }
            set { }
        }
        public string Message { get; set; }

        public string TransactionNumber { get; set; }
    }
}
