﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Simplexity.Traffic.DTOs;

namespace Simplexity.Traffic.DTOs
{
    
    public class ResponseDTO
    {
        
        public ResponseDTO()
        {
            Message = new MessageDTO();
        }
        [DataMember]
        public MessageDTO Message { get; set; }
    }
}
